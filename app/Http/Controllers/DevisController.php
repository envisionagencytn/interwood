<?php

namespace App\Http\Controllers;

use App\Models\Devis;
use App\Models\Produit;
use App\Models\Sous_devis;
use App\Models\Parametre;
use Illuminate\Http\Request;
use App\Models\User;
use Auth ;

class DevisController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roleadmin = User::find(Auth::user()->id);
        if(stristr($roleadmin->idRole, "6") === false){
           return redirect('/');
        } 

        $devis = Devis::orderBy('id', 'DESC')->get();

        return view('admin/devis/dashboard', compact('devis','roleadmin','roleadmin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roleadmin = User::find(Auth::user()->id);
        if(stristr($roleadmin->idRole, "6") === false){
           return redirect('/');
        } 

        $devis = Devis::get();
        $produits = Produit::get();

        //dd($produits[0]->sousproduit);
        return view('admin/devis/add_devis', compact('devis','produits','roleadmin'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd ($request->all());
        
        $lastdevis = Devis::orderBy('created_at', 'desc')->first();

        $int = (int)$lastdevis->devis_num;

        $devis = new Devis ;
        $devis->devis_num = sprintf("%'02d", $int+1);
        $devis->enterprise_id = request('enterprise_id');
        $devis->devis_date = request('devis_date');
        $devis->devis_annee = date("Y");


        $devis->delivery_date = request('delivery_date');
        $devis->gross_weight = is_null(request('gross_weight')) ? 0 : request('gross_weight') ;
        $devis->net_weight = is_null(request('net_weight')) ? 0 : request('net_weight') ;
        $devis->number_pallets = is_null(request('number_pallets')) ? 0 : request('number_pallets') ;
        $devis->size_pallets = is_null(request('size_pallets')) ? 0 : request('size_pallets') ;
        $devis->fumigation = is_null(request('fumigation')) ? 0 : request('fumigation') ;
        $devis->engraving = is_null(request('engraving')) ? 0 : request('engraving') ;
        $devis->package = is_null(request('package')) ? 0 : request('package') ;
        $devis->shipment_insurance = is_null(request('shipment_insurance')) ? 0 : request('shipment_insurance') ;
        $devis->transit = is_null(request('transit')) ? 0 : request('transit') ;
        $devis->taxe = is_null(request('taxe')) ? 0 : request('taxe') ;
        $devis->somme_final_tn = "0";

        $devis->transport = request('transport');
        $devis->delivery = request('delivery');
        $devis->libre = request('libre');

        $devis->save();

        $sous_devis = Sous_devis::where('devis_id',null)->get() ;

        //dd($sous_devis);
        foreach ($sous_devis as $sousdevis ) {
            $sousdevis->devis_id = $devis->id;
            $sousdevis->save();
        }
       
        

        return redirect('my_admin/devis');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Devis  $devis
     * @return \Illuminate\Http\Response
     */
    public function show(Devis $devis)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Devis  $devis
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roleadmin = User::find(Auth::user()->id);
        if(stristr($roleadmin->idRole, "6") === false){
           return redirect('/');
        } 

        $devis = Devis::find($id);
        $sousdevis = Sous_devis::where('devis_id',$id)->get();
        $produits = Produit::get();

        //dd($sousdevis);

        return view('admin/devis/edit_devis', compact('devis','produits','sousdevis','roleadmin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Devis  $devis
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Devis $id)
    {

        $devis = Devis::find($id->id); 

        //dd ($request->all());

        $devis->enterprise_id = request('enterprise_id');
        $devis->devis_date = request('devis_date');
        $devis->devis_annee = date("Y");

        $devis->delivery_date = request('delivery_date');
        $devis->gross_weight = is_null(request('gross_weight')) ? 0 : request('gross_weight') ;
        $devis->net_weight = is_null(request('net_weight')) ? 0 : request('net_weight') ;
        $devis->number_pallets = is_null(request('number_pallets')) ? 0 : request('number_pallets') ;
        $devis->size_pallets = is_null(request('size_pallets')) ? 0 : request('size_pallets') ;
        $devis->fumigation = is_null(request('fumigation')) ? 0 : request('fumigation') ;
        $devis->engraving = is_null(request('engraving')) ? 0 : request('engraving') ;
        $devis->package = is_null(request('package')) ? 0 : request('package') ;
        $devis->shipment_insurance = is_null(request('shipment_insurance')) ? 0 : request('shipment_insurance') ;
        $devis->transit = is_null(request('transit')) ? 0 : request('transit') ;
        $devis->taxe = is_null(request('taxe')) ? 0 : request('taxe') ;
        $devis->somme_final_tn = "0";

        $devis->transport = request('transport');
        $devis->delivery = request('delivery');
        $devis->libre = request('libre');

        $devis->save();



        return redirect('my_admin/devis');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Devis  $devis
     * @return \Illuminate\Http\Response
     */
    public function destroy(Devis $devis)
    {
        //
    }

    public function print($id)
    {
        $devis = Devis::find($id);
        $sousdevis = Sous_devis::where('devis_id',$id)->get();
        $parametre = Parametre::find('1');
        //dd($sousdevis);

        return view('admin/devis/print', compact('devis','sousdevis','parametre'));
    }

    public function adddetail()
    {
        return view('admin/devis/add_devis_details');
    }

    public function adddetailpost(Request $request)
    {

        $sous_devis = new Sous_devis;
        $sous_devis->devis_id = null;
        $sous_devis->produit_id = $request->get('produit_id');
        $sous_devis->devis_details_qte = $request->get('devis_details_qte');
        $sous_devis->devis_details_prix = $request->get('devis_details_prix');
        $sous_devis->devis_remise = $request->get('devis_remise');
        $sous_devis->devis_taux = $request->get('devis_taux');
        $sous_devis->devis_devise = $request->get('devis_devise');
    
        $sous_devis->save();

        return redirect('/my_admin/devis/detail/edit/'.$sous_devis->id);

    }

    public function editdetail($id)
    {

        $sousdevis = Sous_devis::find($id);
        return view('admin/devis/edit_devis_details', compact('sousdevis'));
    }

    public function editdetailpost(Request $request , $id)
    {
        $sous_devis = Sous_devis::find($id);
        $sous_devis->produit_id = $request->get('produit_id');
        $sous_devis->devis_details_qte = $request->get('devis_details_qte');
        $sous_devis->devis_details_prix = $request->get('devis_details_prix');
        $sous_devis->devis_remise = $request->get('devis_remise');
        $sous_devis->devis_taux = $request->get('devis_taux');
        $sous_devis->devis_devise = $request->get('devis_devise');
    
        $sous_devis->save();

        return redirect('/my_admin/devis/detail/edit/'.$sous_devis->id);

    }

}
