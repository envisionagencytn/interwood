<?php

namespace App\Http\Controllers;

use App\Models\Enterprise;
use App\Models\User;

use Illuminate\Http\Request;

use Illuminate\Database\Eloquent\Model;
use DB ;
use Storage;
use Auth ;

class EnterpriseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');       
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $roleadmin = User::find(Auth::user()->id);
        if(stristr($roleadmin->idRole, "3") === false){
           return redirect('/');
        } 

        $enterprise = Enterprise::orderBy('id', 'desc')->get();

        //dd($enterprise->user );
        return view('admin/enterprises/dashboard', compact('enterprise','roleadmin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roleadmin = User::find(Auth::user()->id);
        if(stristr($roleadmin->idRole, "3") === false){
           return redirect('/');
        } 
        $enterprises = Enterprise::get();

        return view('admin/enterprises/add_enterprises', compact('enterprises','roleadmin'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user = new User ;
        $user->name = request('name');
        $user->email = request('email');
        $user->password = "";
        $user->portable = request('portable');
        $user->portable2 = request('portable2'," ");
        $user->fax = request('fax'," ");
        $user->mf = request('mf'," ");

        $user->adresse = request('adresse');
        $user->pays = request('pays');
 
        $user->save();

        // create a new user object
        $enterprise = new Enterprise;
        $enterprise->nom = request('nom');
        $enterprise->user_id = $user->id;

        $enterprise->save();
        // redirect back to the users list
        return redirect('my_admin/enterprises');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Enterprise  $enterprise
     * @return \Illuminate\Http\Response
     */
    public function show(Enterprise $enterprise)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Enterprise  $enterprise
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roleadmin = User::find(Auth::user()->id);
        if(stristr($roleadmin->idRole, "3") === false){
           return redirect('/');
        } 
        $enterprises = Enterprise::find($id);
      return view('admin/enterprises/edit_enterprises', compact('enterprises','roleadmin'));
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Enterprise  $enterprise
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Enterprise $id)
    {
        $enterprise = Enterprise::find($id->id);
        $user = User::find($enterprise->user_id);

        $user->name = request('name');
        $user->email = request('email');
        $user->portable = request('portable');
        $user->portable2 = request('portable2'," ");
        $user->fax = request('fax'," ");
        $user->mf = request('mf'," ");
        
        $user->adresse = request('adresse');
        $user->pays = request('pays');
        $user->save();

        $enterprise->nom = request('nom');

        $enterprise->save();
        // redirect back to the users list
        return redirect('my_admin/enterprises');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Enterprise  $enterprise
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $enterprise = Enterprise::find($id);
        $enterprise->user->delete();        
        return redirect('my_admin/enterprises');
    }
}
