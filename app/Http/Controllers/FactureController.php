<?php

namespace App\Http\Controllers;


use App\Models\Facture;
use App\Models\Produit;
use App\Models\Sous_facture;
use App\Models\Produit_facture;
use App\Models\Parametre;
use App\Models\Sous_Produit;
use Illuminate\Http\Request;
use App\Models\User;
use Auth ;


class FactureController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roleadmin = User::find(Auth::user()->id);
        if(stristr($roleadmin->idRole, "7") === false){
           return redirect('/');
        } 

        $factures = Facture::orderBy('id', 'DESC')->get();

        return view('admin/factures/dashboard', compact('factures','roleadmin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roleadmin = User::find(Auth::user()->id);
        if(stristr($roleadmin->idRole, "7") === false){
           return redirect('/');
        } 

        $factures = Facture::get();
        $produits = Produit::get();

        return view('admin/factures/add_facture', compact('factures','produits','roleadmin'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $lastfacture = Facture::orderBy('created_at', 'desc')->first();
        $int = (int)$lastfacture->facture_num;

        $factures = new Facture ;
        $factures->facture_num = sprintf("%'02d", $int+1);
        $factures->enterprise_id = request('enterprise_id');
        $factures->facture_date = request('facture_date');
        $factures->facture_annee = date("Y");


        $factures->delivery_date = request('delivery_date');
        $factures->gross_weight = is_null(request('gross_weight')) ? 0 : request('gross_weight') ;
        $factures->net_weight = is_null(request('net_weight')) ? 0 : request('net_weight') ;
        $factures->number_pallets = is_null(request('number_pallets')) ? 0 : request('number_pallets') ;
        $factures->size_pallets = is_null(request('size_pallets')) ? 0 : request('size_pallets') ;
        $factures->fumigation = is_null(request('fumigation')) ? 0 : request('fumigation') ;
        $factures->engraving = is_null(request('engraving')) ? 0 : request('engraving') ;
        $factures->package = is_null(request('package')) ? 0 : request('package') ;
        $factures->shipment_insurance = is_null(request('shipment_insurance')) ? 0 : request('shipment_insurance') ;
        $factures->transit = is_null(request('transit')) ? 0 : request('transit') ;
        $factures->taxe = is_null(request('taxe')) ? 0 : request('taxe') ;
        $factures->somme_final_tn = "0";

        $factures->transport = request('transport');
        $factures->delivery = request('delivery');
        $factures->libre = request('libre');

      
        
        $factures->save();


        $sous_facture = Sous_facture::where('facture_id',null)->get() ;

        //dd($sous_devis);
        foreach ($sous_facture as $sousfacture ) {
            $sousfacture->facture_id = $factures->id;
            $sousfacture->save();
        }
       

        return redirect('my_admin/factures');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Facture  $facture
     * @return \Illuminate\Http\Response
     */
    public function show(Facture $facture)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Facture  $facture
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roleadmin = User::find(Auth::user()->id);
        if(stristr($roleadmin->idRole, "7") === false){
           return redirect('/');
        } 

        $factures = Facture::find($id);
        $sousfactures = Sous_facture::where('facture_id',$id)->get();
        $produits = Produit::get();

        //dd($sousfacture);

        return view('admin/factures/edit_facture', compact('factures','produits','sousfactures','roleadmin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Facture  $facture
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Facture $id)
    {
        $factures = Facture::find($id->id); ;
        $factures->enterprise_id = request('enterprise_id');
        $factures->facture_date = request('facture_date');
        $factures->facture_annee = date("Y");

        $factures->delivery_date = request('delivery_date');
        $factures->gross_weight = is_null(request('gross_weight')) ? 0 : request('gross_weight') ;
        $factures->net_weight = is_null(request('net_weight')) ? 0 : request('net_weight') ;
        $factures->number_pallets = is_null(request('number_pallets')) ? 0 : request('number_pallets') ;
        $factures->size_pallets = is_null(request('size_pallets')) ? 0 : request('size_pallets') ;
        $factures->fumigation = is_null(request('fumigation')) ? 0 : request('fumigation') ;
        $factures->engraving = is_null(request('engraving')) ? 0 : request('engraving') ;
        $factures->package = is_null(request('package')) ? 0 : request('package') ;
        $factures->shipment_insurance = is_null(request('shipment_insurance')) ? 0 : request('shipment_insurance') ;
        $factures->transit = is_null(request('transit')) ? 0 : request('transit') ;
        $factures->taxe = is_null(request('taxe')) ? 0 : request('taxe') ;
        $factures->somme_final_tn = "0";

        $factures->transport = request('transport');
        $factures->delivery = request('delivery');
        $factures->libre = request('libre');

        $factures->save();

        

        return redirect('my_admin/factures');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Facture  $facture
     * @return \Illuminate\Http\Response
     */
    public function destroy(Facture $facture)
    {
        //
    }

    public function print($id)
    {
        $factures = Facture::find($id);
        $sousfactures = Sous_facture::where('facture_id',$id)->get();
        $parametre = Parametre::find('1');

        return view('admin/factures/print', compact('factures','sousfactures','parametre'));
    }

    public function adddetail()
    {
        
        $produits = Produit::get();

        return view('admin/factures/add_facture_details',compact('produits'));
    }

    public function adddetailpost(Request $request)
    {

       

        $sousproduits = Sous_Produit::where('produit_id',$request->get('produit_id'))->where('quantite_restant','!=' , 0)->get();

        $qte = $request->get('facture_details_qte') ;
        foreach ($sousproduits as $sousproduit ) {
            if ($qte >= 0) {
               
                if ($sousproduit->quantite_restant < $qte) {
                    
                }
                
                $qte = $qte - $sousproduit->quantite_restant;
                
                if ($qte > 0) {
                    $sousprodmois = Sous_Produit::find($sousproduit->id);
                    $sousprodmois->quantite_restant = 0;
                    $sousprodmois->save();
                    //print_r( "93ad ". 0 ."<br>");
                } else {
                    //print_r( "Mazel ". abs($qte)."<br>" );
                    $sousprodmois = Sous_Produit::find($sousproduit->id);
                    $sousprodmois->quantite_restant = abs($qte);
                    $sousprodmois->save();
                }
                
                                
            }
            //print_r("ba3ed el if $qte <br>");
        }
        
        $prodfact = Produit_facture::where('produit_id',$request->get('produit_id'))->first() ;
        $prodfact->quantite = $prodfact->quantite - $request->get('facture_details_qte');
        $prodfact->save();

        //dd($sousproduit);
        $sous_facture = new Sous_facture;
        $sous_facture->facture_id = null;
        $sous_facture->produit_id = $request->get('produit_id');
        $sous_facture->facture_details_qte = $request->get('facture_details_qte');
        $sous_facture->facture_details_prix = $request->get('facture_details_prix');
        $sous_facture->facture_remise = $request->get('facture_remise');
        $sous_facture->facture_taux = $request->get('facture_taux');
        $sous_facture->facture_devise = $request->get('facture_devise');
        $sous_facture->pmp = "0";
        $sous_facture->facture_somme_final_tn = "0";

        $sous_facture->save();

        return redirect('/my_admin/factures/detail/edit/'.$sous_facture->id);

    }

    public function editdetail($id)
    {

        $sousfacture = Sous_facture::find($id);

        $sousfacdeta = Produit_facture::where('produit_id',$sousfacture->produit_id)->first(); 

        //dd($sousfacdeta);

        return view('admin/factures/edit_facture_details', compact('sousfacture','sousfacdeta'));
    }

    public function editdetailpost(Request $request , $id)
    {
        $sous_facture = Sous_facture::find($id);
        $sous_facture->produit_id = $request->get('produit_id');
        $sous_facture->facture_details_qte = $request->get('facture_details_qte');
        $sous_facture->facture_details_prix = $request->get('facture_details_prix');
        $sous_facture->facture_remise = $request->get('facture_remise');
        $sous_facture->facture_taux = $request->get('facture_taux');
        $sous_facture->facture_devise = $request->get('facture_devise');
    
        $sous_facture->save();

        return redirect('/my_admin/factures/detail/edit/'.$sous_facture->id);

    }

}
