<?php

namespace App\Http\Controllers;

use App\Models\Famille;
use Illuminate\Http\Request;
use App\Models\User;
use Auth ;


class FamilleController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $roleadmin = User::find(Auth::user()->id);
        if(stristr($roleadmin->idRole, "5") === false){
           return redirect('/');
        } 

        $familles = Famille::orderBy('id', 'desc')->get();

        //dd($familles );
        return view('admin/produits/famille/dashboard', compact('familles','roleadmin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roleadmin = User::find(Auth::user()->id);
        if(stristr($roleadmin->idRole, "5") === false){
           return redirect('/');
        } 

        $familles = Famille::get();

        return view('admin/produits/famille/add_familles', compact('familles','roleadmin'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $famille = new Famille;
         $famille->nom = request('nom');
         $famille->identifiant = request('identifiant');
         $famille->dernier_code = "0";
 
         $famille->save();
         // redirect back to the users list
         return redirect('my_admin/produits/categorie');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Famille  $famille
     * @return \Illuminate\Http\Response
     */
    public function show(Famille $famille)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Famille  $famille
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roleadmin = User::find(Auth::user()->id);
        if(stristr($roleadmin->idRole, "5") === false){
           return redirect('/');
        } 
        $familles = Famille::find($id);

        return view('admin/produits/famille/edit_familles', compact('familles','roleadmin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Famille  $famille
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Famille $id)
    {
        $famille = Famille::find($id->id);
        $famille->nom = request('nom');
        $famille->identifiant = request('identifiant');

        $famille->save();
        // redirect back to the users list
        return redirect('my_admin/produits/categorie');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Famille  $famille
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $famille = Famille::find($id);
        $famille->delete();        
        return redirect('my_admin/produits/categorie');
    }
}
