<?php

namespace App\Http\Controllers;

use App\Models\Fournisseur;
use App\Models\User;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use DB ;
use Storage;
use Auth;


class FournisseurController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $roleadmin = User::find(Auth::user()->id);
        if(stristr($roleadmin->idRole, "4") === false){
           return redirect('/');
        } 

        $fournisseur = Fournisseur::orderBy('id', 'desc')->get();

        //print_r ($page );
        return view('admin/fournisseurs/dashboard', compact('fournisseur','roleadmin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roleadmin = User::find(Auth::user()->id);
        if(stristr($roleadmin->idRole, "4") === false){
           return redirect('/');
        } 
        $fournisseurs = fournisseur::get();

        return view('admin/fournisseurs/add_fournisseurs', compact('fournisseurs','roleadmin'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User ;
        $user->name = request('name');
        if (request('email') ) {
            $user->email = request('email');
        } else {
            $user->email = " ";
        }        
        $user->password = "";
        $user->portable = request('portable');
        $user->portable2 = request('portable2'," ");
        $user->fax = request('fax'," ");
        $user->mf = request('mf'," ");

        $user->adresse = request('adresse');
        $user->pays = request('pays');
 
        $user->save();

        // create a new user object
        $fournisseur = new fournisseur;
        $fournisseur->nom = request('nom');
        $fournisseur->domaineactivite = request('domaineactivite');
       
        $fournisseur->user_id = $user->id;

        $fournisseur->save();
        // redirect back to the users list
        return redirect('my_admin/fournisseurs');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Fournisseur  $fournisseur
     * @return \Illuminate\Http\Response
     */
    public function show(Fournisseur $fournisseur)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Fournisseur  $fournisseur
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roleadmin = User::find(Auth::user()->id);
        if(stristr($roleadmin->idRole, "4") === false){
           return redirect('/');
        } 

        $fournisseurs = Fournisseur::find($id);
        return view('admin/fournisseurs/edit_fournisseurs', compact('fournisseurs','roleadmin'));
         
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Fournisseur  $fournisseur
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Fournisseur $id)
    {
        $fournisseur = Fournisseur::find($id->id);
        $user = User::find($fournisseur->user_id);

        $user->name = request('name');
        if (request('email') ) {
            $user->email = request('email');
        } else {
            $user->email = " ";
        }    
        $user->portable = request('portable');
        $user->portable2 = request('portable2'," ");
        $user->fax = request('fax'," ");
        $user->mf = request('mf'," ");
        
        $user->adresse = request('adresse');
        $user->pays = request('pays');
        $user->save();

        $fournisseur->nom = request('nom');
        $fournisseur->domaineactivite = request('domaineactivite');

        $fournisseur->save();
        // redirect back to the users list
        return redirect('my_admin/fournisseurs');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Fournisseur  $fournisseur
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fournisseur = Fournisseur::find($id);

        //dd($fournisseur->user);
        $fournisseur->user->delete();        
        return redirect('my_admin/fournisseurs');
    }
}
