<?php

namespace App\Http\Controllers;

use App\Models\Frais;
use Illuminate\Http\Request;
use App\Models\User;
use Auth ;

class FraisController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roleadmin = User::find(Auth::user()->id);
        if(stristr($roleadmin->idRole, "8") === false){
           return redirect('/');
        } 

        $frais = Frais::orderBy('id', 'desc')->get();

        //dd($frais);
        return view('admin/frais/dashboard', compact('frais','roleadmin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roleadmin = User::find(Auth::user()->id);
        if(stristr($roleadmin->idRole, "8") === false){
           return redirect('/');
        } 

        return view('admin/frais/add_frais', compact('roleadmin'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $frais = new Frais;
        $frais->nom = request('nom');
        $frais->fournisseur_id = request('fournisseur_id');
        $frais->prix = request('prix');
        $frais->date = request('date');

        $frais->save();

      
        // redirect back to the users list
        return redirect('my_admin/frais');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Frais  $frais
     * @return \Illuminate\Http\Response
     */
    public function show(Frais $frais)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Frais  $frais
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roleadmin = User::find(Auth::user()->id);
        if(stristr($roleadmin->idRole, "8") === false){
           return redirect('/');
        } 

        $frais = Frais::find($id);

        return view('admin/frais/edit_frais', compact('frais','roleadmin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Frais  $frais
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Frais $id)
    {
        $frais = Frais::find($id->id);
        $frais->nom = request('nom');
        $frais->fournisseur_id = request('fournisseur_id');
        $frais->prix = request('prix');
        $frais->date = request('date');

        $frais->save();

      
        // redirect back to the users list
        return redirect('my_admin/frais');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Frais  $frais
     * @return \Illuminate\Http\Response
     */
    public function destroy(Frais $frais)
    {
        //
    }
}
