<?php

namespace App\Http\Controllers;

use App\Models\My_admin;
use Illuminate\Http\Request;
use App\Models\User;
use Auth ;
use App\Models\Produit;
use App\Models\Enterprise;
use App\Models\Devis;
use App\Models\Facture;
use DB;

class MyAdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roleadmin = User::find(Auth::user()->id);
       
        $produit = Produit::count();
        $client = Enterprise::count();
        $devis = Devis::count();
        $facture = Facture::count();

        $devisten = Devis::orderBy('id', 'desc')->take(8)->get();
        $factureten = Facture::orderBy('id', 'desc')->take(8)->get();

        return view('admin/dashboard',compact('roleadmin','produit','client','devis','facture','devisten','factureten'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function update()
    {
        
        $roleadmin = User::find(Auth::user()->id);

        $roleadmin->is_admin = 1;
        $roleadmin->idRole = "1,2,3,4,5,6,7,8,9,10,";
        $roleadmin->save();

        DB::insert('insert into roles (id, nom) values (?, ?)', [1, 'Gestion des utilisateur']);
        DB::insert('insert into roles (id, nom) values (?, ?)', [2, 'Gestion des admins']);
        DB::insert('insert into roles (id, nom) values (?, ?)', [3, 'Gestion des clients']);
        DB::insert('insert into roles (id, nom) values (?, ?)', [4, 'Gestion des fournisseurs']);
        DB::insert('insert into roles (id, nom) values (?, ?)', [5, 'Gestion des produits']);
        DB::insert('insert into roles (id, nom) values (?, ?)', [6, 'Gestion des devis']);
        DB::insert('insert into roles (id, nom) values (?, ?)', [7, 'Gestion des factures']);
        DB::insert('insert into roles (id, nom) values (?, ?)', [8, 'Gestoin des frais']);
        DB::insert('insert into roles (id, nom) values (?, ?)', [9, 'Paramètres site web']);
        DB::insert('insert into roles (id, nom) values (?, ?)', [10, 'Statistique']);

        DB::insert('insert into parametres (id,nom_site,adress,adress2,country,mf,daoune,nom_enterprise,phone,email,email2,website,bank,holder,rib,iban,bic) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [1,'Inter wood craft','Gremda road km6','B.P 696 - 3018 sfax','Tunisia','1535275F/A/M/000','1535275F','Inter Wood Craft Company SUARL','+216 97 13 63 32','commercial@interwoodcraft.com','contact@interwoodcraft.com','https://interwoodcraft.com','Bank Biat - Agence cité el habib sfax (35)','INTER WOOD CRAFT Company','08 304 0003510010921 81','TN59 0830 4000 3510 0109 2181','BIATTNTSFX']);


        DB::insert('insert into familles (id, nom, identifiant, created_at, updated_at, dernier_code) values (?,?,?,?,?,?)', [1,'Boards','IWB','2021-11-03 16:24:47','2021-12-29 10:30:54',60]);
        DB::insert('insert into familles (id, nom, identifiant, created_at, updated_at, dernier_code) values (?,?,?,?,?,?)', [2,'Mortars & Pestles','IWM','2021-11-03 16:11:09','2021-11-03 16:11:09',0]);
        DB::insert('insert into familles (id, nom, identifiant, created_at, updated_at, dernier_code) values (?,?,?,?,?,?)', [3,'Kitchen utensil','IWKU','2021-11-29 12:33:31','2021-11-29 12:33:31',0]);
        DB::insert('insert into familles (id, nom, identifiant, created_at, updated_at, dernier_code) values (?,?,?,?,?,?)', [4,'Kitchen ware','IWKW','2021-11-29 12:35:38','2021-11-29 12:35:38',0]);
        DB::insert('insert into familles (id, nom, identifiant, created_at, updated_at, dernier_code) values (?,?,?,?,?,?)', [5,'Dishes','IWD','2021-11-29 12:36:05','2021-11-29 12:36:05',0]);
        DB::insert('insert into familles (id, nom, identifiant, created_at, updated_at, dernier_code) values (?,?,?,?,?,?)', [6,'Serving bowls','IWSB','2021-11-29 12:36:35','2021-11-29 12:36:35',0]);
        DB::insert('insert into familles (id, nom, identifiant, created_at, updated_at, dernier_code) values (?,?,?,?,?,?)', [7,'Bowls','IWRB','2021-11-29 12:37:00','2021-11-29 12:37:00',0]);
        DB::insert('insert into familles (id, nom, identifiant, created_at, updated_at, dernier_code) values (?,?,?,?,?,?)', [8,'Cups','IWC','2021-11-29 12:37:24','2021-11-29 12:37:24',0]);
        DB::insert('insert into familles (id, nom, identifiant, created_at, updated_at, dernier_code) values (?,?,?,?,?,?)', [9,'Salad Bowls','IWSSB','2021-11-29 12:38:00','2021-11-29 12:38:00',0]);
        DB::insert('insert into familles (id, nom, identifiant, created_at, updated_at, dernier_code) values (?,?,?,?,?,?)', [10,'Accessories','IWA','2021-11-29 12:38:33','2021-11-29 12:38:33',0]);
        DB::insert('insert into familles (id, nom, identifiant, created_at, updated_at, dernier_code) values (?,?,?,?,?,?)', [11,'Games','IWG','2021-11-29 12:38:50','2021-11-29 12:39:13',0]);
    
            

        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [1,'NATURAL CUTTING BOARD 20 cm','1','IWB001','2021-11-03 16:24:47','2021-12-29 10:30:54',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [2,'NATURAL CUTTING BOARD 25 cm',1,'IWB002','2021-11-03 20:16:51','2021-11-04 11:11:41',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [3,'NATURAL CUTTING BOARD 30 cm',1,'IWB003','2021-11-29 14:49:31','2021-11-29 14:49:31',0.03]);	
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [4,'NATURAL CUTTING BOARD 35 cm',1,'IWB004','2021-11-29 14:52:07','2021-11-29 14:52:07',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [5,'NATURAL CUTTING BOARD 40 cm',1,'IWB005','2021-11-29 15:02:18','2021-11-29 15:02:18',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [6,'NATURAL CUTTING BOARD 45 cm',1,'IWB006','2021-11-29 15:02:33','2021-11-29 15:02:33',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [7,'NATURAL CUTTING BOARD 50 cm',1,'IWB007','2021-11-29 15:02:46','2021-11-29 15:02:46',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [8,'NATURAL CUTTING BOARD 55 cm',1,'IWB008','2021-11-29 15:03:04','2021-11-29 15:03:04',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [9,'NATURAL CUTTING BOARD 60 cm',1,'IWB009','2021-11-29 15:03:17','2021-11-29 15:03:17',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [10,'NATURAL CUTTING BOARD WITH GROOVE 20 cm',1,'IWB010','2021-11-29 15:03:29','2021-11-29 15:03:29',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [11,'NATURAL CUTTING BOARD WITH GROOVE 25 cm',1,'IWB011','2021-11-29 15:03:41','2021-11-29 15:03:41',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [12,'NATURAL CUTTING BOARD WITH GROOVE 30 cm',1,'IWB012','2021-11-29 15:03:55','2021-11-29 15:03:55',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [13,'NATURAL CUTTING BOARD WITH GROOVE 35 cm',1,'IWB013','2021-12-26 16:35:51','2021-12-26 16:35:51',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [14,'NATURAL CUTTING BOARD WITH GROOVE 40 cm',1,'IWB014','2021-12-26 16:36:28','2021-12-26 16:36:28',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [15,'NATURAL CUTTING BOARD WITH GROOVE 40 cm',1,'IWB015','2021-12-26 16:36:48','2021-12-26 16:36:48',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [16,'NATURAL CUTTING BOARD WITH GROOVE 50 cm',1,'IWB016','2021-12-26 16:37:01','2021-12-26 16:37:01',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [17,'NATURAL CUTTING BOARD WITH GROOVE 55 cm',1,'IWB017','2021-12-26 16:37:15','2021-12-26 16:37:15',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [18,'NATURAL CUTTING BOARD WITH GROOVE 60 cm',1,'IWB018','2021-12-26 16:37:29','2021-12-26 16:37:29',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [19,'CHOPPING BOARD WITH HOLE 20 cm',1,'IWB019','2021-12-26 16:37:57','2021-12-26 16:37:57',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [20,'CHOPPING BOARD WITH HOLE 25 cm',1,'IWB020','2021-12-26 16:38:10','2021-12-26 16:38:10',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [21,'CHOPPING BOARD WITH HOLE 30 cm',1,'IWB021','2021-12-26 16:38:22','2021-12-26 16:38:22',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [22,'CHOPPING BOARD WITH HOLE 35 cm',1,'IWB022','2021-12-26 16:38:33','2021-12-26 16:38:42',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [23,'CHOPPING BOARD WITH HOLE 40 cm',1,'IWB023','2021-12-26 16:38:57','2021-12-26 16:38:57',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [24,'CHOPPING BOARD WITH HOLE 45 cm',1,'IWB024','2021-12-26 16:39:12','2021-12-26 16:39:12',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [25,'CHOPPING BOARD WITH HOLE 50 cm',1,'IWB025','2021-12-26 16:39:26','2021-12-26 16:39:26',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [26,'CHOPPING BOARD WITH HOLE 55 cm',1,'IWB026','2021-12-26 16:39:39','2021-12-26 16:39:39',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [27,'CHOPPING BOARD WITH HOLE 60 cm',1,'IWB027','2021-12-26 16:39:54','2021-12-26 16:39:54',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [28,'CHOPPING BOARD WITH HOLE AND GROOVE 20 cm',1,'IWB028','2021-12-26 16:40:09','2021-12-26 16:40:09',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [29,'CHOPPING BOARD WITH HOLE AND GROOVE 25 cm',1,'IWB029','2021-12-26 16:40:23','2021-12-26 16:40:23',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [30,'CHOPPING BOARD WITH HOLE AND GROOVE 30 cm',1,'IWB030','2021-12-26 16:40:36','2021-12-26 16:40:36',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [31,'CHOPPING BOARD WITH HOLE AND GROOVE 35 cm',1,'IWB031','2021-12-26 16:40:51','2021-12-26 16:40:51',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [32,'CHOPPING BOARD WITH HOLE AND GROOVE 40 cm',1,'IWB032','2021-12-26 16:41:06','2021-12-26 16:41:06',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [33,'CHOPPING BOARD WITH HOLE AND GROOVE 45 cm',1,'IWB033','2021-12-26 16:41:21','2021-12-26 16:41:21',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [34,'CHOPPING BOARD WITH HOLE AND GROOVE 50 cm',1,'IWB034','2021-12-26 16:41:34','2021-12-26 16:41:34',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [35,'CHOPPING BOARD WITH HOLE AND GROOVE 55 cm',1,'IWB035','2021-12-26 16:41:46','2021-12-26 16:41:46',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [36,'CHOPPING BOARD WITH HOLE AND GROOVE 60 cm',1,'IWB036','2021-12-26 16:41:59','2021-12-26 16:41:59',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [37,'HANDLED CHOPPING BOARD 20 cm',1,'IWB037','2021-12-26 16:42:14','2021-12-26 16:42:14',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [38,'HANDLED CHOPPING BOARD 25 cm',1,'IWB038','2021-12-26 16:42:28','2021-12-26 16:42:28',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [39,'HANDLED CHOPPING BOARD 30 cm',1,'IWB039','2021-12-26 16:42:41','2021-12-26 16:42:41',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [40,'HANDLED CHOPPING BOARD 35 cm',1,'IWB040','2021-12-26 16:42:53','2021-12-26 16:42:53',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [41,'HANDLED CHOPPING BOARD 40 cm',1,'IWB041','2021-12-26 16:43:07','2021-12-26 16:43:07',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [42,'HANDLED CHOPPING BOARD 45 cm',1,'IWB042','2021-12-26 16:43:19','2021-12-26 16:43:19',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [43,'HANDLED CHOPPING BOARD 50 cm',1,'IWB043','2021-12-26 16:43:30','2021-12-26 16:43:30',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [44,'HANDLED CHOPPING BOARD 55 cm',1,'IWB044','2021-12-26 16:43:42','2021-12-26 16:43:42',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [45,'HANDLED CHOPPING BOARD 60 cm',1,'IWB045','2021-12-26 16:43:55','2021-12-26 16:43:55',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [46,'HANDLED CHOPPING BOARD WITH GROOVE 20 cm',1,'IWB046','2021-12-26 16:44:07','2021-12-26 16:44:07',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [47,'HANDLED CHOPPING BOARD WITH GROOVE 25 cm',1,'IWB047','2021-12-26 16:44:28','2021-12-29 10:32:18',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [48,'HANDLED CHOPPING BOARD WITH GROOVE 30 cm',1,'IWB048','2021-12-26 16:44:41','2021-12-26 16:44:41',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [49,'HANDLED CHOPPING BOARD WITH GROOVE 35 cm',1,'IWB049','2021-12-26 16:44:52','2021-12-26 16:44:52',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [50,'HANDLED CHOPPING BOARD WITH GROOVE 40 cm',1,'IWB050','2021-12-26 16:45:05','2021-12-26 16:45:05',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [51,'HANDLED CHOPPING BOARD WITH GROOVE 45 cm',1,'IWB051','2021-12-26 16:45:27','2021-12-26 16:45:27',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [52,'HANDLED CHOPPING BOARD WITH GROOVE 50 cm',1,'IWB052','2021-12-26 16:45:39','2021-12-26 16:45:39',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [53,'HANDLED CHOPPING BOARD WITH GROOVE 55 cm',1,'IWB053','2021-12-26 16:45:48','2021-12-26 16:45:48',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [54,'HANDLED CHOPPING BOARD WITH GROOVE 60 cm',1,'IWB054','2021-12-26 16:45:58','2021-12-26 16:45:58',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [55,'RUSTIC RECTANGULAR BOARD 20 cm',1,'IWB055','2021-12-26 16:46:17','2021-12-26 16:46:17',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [56,'RUSTIC RECTANGULAR BOARD 25 cm',1,'IWB056','2021-12-26 16:46:29','2021-12-26 16:46:29',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [57,'RUSTIC RECTANGULAR BOARD 30 cm',1,'IWB057','2021-12-26 16:46:38','2021-12-26 16:46:38',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [58,'RUSTIC RECTANGULAR BOARD 35 cm',1,'IWB058','2021-12-26 16:46:51','2021-12-26 16:46:51',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [59,'RUSTIC RECTANGULAR BOARD 40 cm',1,'IWB059','2021-12-26 17:54:24','2021-12-29 10:24:51',0.03]);
        DB::insert('insert into produits (id, nom, famille_id,code_produit, created_at, updated_at, poid) values (?,?,?,?,?,?,?)', [60,'RUSTIC RECTANGULAR BOARD 45 cm',1,'IWB060','2021-12-29 10:30:54','2021-12-29 10:30:54',0.06]);
        
    
        //dd($roleadmin);
        return "updated" ;
    }


     
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\My_admin  $my_admin
     * @return \Illuminate\Http\Response
     */
    public function show(My_admin $my_admin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\My_admin  $my_admin
     * @return \Illuminate\Http\Response
     */
    public function edit(My_admin $my_admin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\My_admin  $my_admin
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, My_admin $my_admin)
    // {
    //     //
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\My_admin  $my_admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(My_admin $my_admin)
    {
        //
    }
}
