<?php

namespace App\Http\Controllers;

use App\Models\Parametre;
use Illuminate\Http\Request;
use App\Models\User;
use Auth ;

class ParametreController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roleadmin = User::find(Auth::user()->id);
        if(stristr($roleadmin->idRole, "9") === false){
           return redirect('/');
        } 
        $parametre = Parametre::find('1');

        return view('admin/parametre/edit_parametre', compact('roleadmin','parametre'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parametre = Parametre::find('1');
        $parametre->nom_site = request('nom_site');
        $parametre->adress = request('adress');
        $parametre->adress2 = request('adress2');
        $parametre->country = request('country');
        $parametre->mf = request('mf');
        $parametre->daoune = request('daoune');
        $parametre->nom_enterprise = request('nom_enterprise');
        $parametre->phone = request('phone');
        $parametre->email = request('email');
        $parametre->email2 = request('email2');
        $parametre->website = request('website');
        $parametre->bank = request('bank');
        $parametre->holder = request('holder');
        $parametre->rib = request('rib');
        $parametre->iban = request('iban');
        $parametre->bic = request('bic');
       
        $parametre->save();
        // redirect back to the users list
        return redirect('my_admin/parametres')->with('status', 'Mise à jour avec succès !');
    }

    public function statistique()
    {
        $roleadmin = User::find(Auth::user()->id);
        if(stristr($roleadmin->idRole, "10") === false){
           return redirect('/');
        } 

        return view('admin/parametre/statistique', compact('roleadmin'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Parametre  $parametre
     * @return \Illuminate\Http\Response
     */
    public function show(Parametre $parametre)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Parametre  $parametre
     * @return \Illuminate\Http\Response
     */
    public function edit(Parametre $parametre)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Parametre  $parametre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Parametre $parametre)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Parametre  $parametre
     * @return \Illuminate\Http\Response
     */
    public function destroy(Parametre $parametre)
    {
        //
    }
}
