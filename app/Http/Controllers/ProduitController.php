<?php

namespace App\Http\Controllers;

use App\Models\Produit;
use App\Models\Famille;
use App\Models\Fournisseur;
use App\Models\Enterprise;
use Illuminate\Http\Request;
use App\Models\User;
use Auth ;


class ProduitController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roleadmin = User::find(Auth::user()->id);
        if(stristr($roleadmin->idRole, "5") === false){
           return redirect('/');
        } 

        $produits = Produit::orderBy('id', 'desc')->paginate(15);

        //dd($produits );
        return view('admin/produits/dashboard', compact('produits','roleadmin',));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roleadmin = User::find(Auth::user()->id);
        if(stristr($roleadmin->idRole, "5") === false){
           return redirect('/');
        } 

        $produits = Produit::get();
        $familles = Famille::get();

        return view('admin/produits/add_produits', compact('produits','familles','roleadmin'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $produit = new Produit;
        $produit->nom = request('nom');
        $produit->famille_id = request('famille_id');
        $familles = Famille::find($produit->famille_id);

        $codenumber = $familles->dernier_code+1 ;
        $code = $familles->identifiant.sprintf("%'03d", $codenumber) ;
        $produit->code_produit = $code;
        $produit->poid = request('poid');

        $produit->save();

        $familles->dernier_code = $familles->dernier_code+1 ;
        $familles->save();
        // redirect back to the users list
        return redirect('my_admin/produits');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function show(Produit $produit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roleadmin = User::find(Auth::user()->id);
        if(stristr($roleadmin->idRole, "5") === false){
           return redirect('/');
        } 

        $produits = Produit::find($id);
        $familles = Famille::get();

        return view('admin/produits/edit_produits', compact('produits','familles','roleadmin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Produit $id)
    {
        $produit = Produit::find($id->id);
        $produit->nom = request('nom');
        $produit->famille_id = request('famille_id');
        $produit->poid = request('poid');

        $produit->save();
        // redirect back to the users list
        return redirect('my_admin/produits')->with('status', 'Profile updated!');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Produit $produit)
    {
        //
    }

    public function produitajax(Request $request)
    {
          $search = $request->get('term');
          $result = Produit::where('nom','like',"%{$search}%")->get();
        
          return response()->json($result);
            
    } 

    public function produitajaxone(Request $request , $id)
    {
          $search = $request->get('term');
          $result = Produit::where('nom','like',"%{$search}%")->find($id);
        
          return response()->json($result);
            
    } 

    public function fournisseurajax(Request $request)
    {
          $search = $request->get('term');
          $result = Fournisseur::where('nom','like',"%{$search}%")->get();
        
          return response()->json($result);
            
    } 
    public function enterpriseajax(Request $request)
    {
          $search = $request->get('term');
          $result = Enterprise::where('nom','like',"%{$search}%")->get();
        
          return response()->json($result);
            
    } 
}
