<?php

namespace App\Http\Controllers;

use App\Models\Produit_facture;
use Illuminate\Http\Request;
use App\Models\User;
use Auth ;


class ProduitFactureController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Produit_facture  $produit_facture
     * @return \Illuminate\Http\Response
     */
    public function show(Produit_facture $produit_facture)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Produit_facture  $produit_facture
     * @return \Illuminate\Http\Response
     */
    public function edit(Produit_facture $produit_facture)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Produit_facture  $produit_facture
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Produit_facture $produit_facture)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Produit_facture  $produit_facture
     * @return \Illuminate\Http\Response
     */
    public function destroy(Produit_facture $produit_facture)
    {
        //
    }
}
