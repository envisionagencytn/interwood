<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use DB;


class RoleController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data['roleadmin'] = User::find(Auth::user()->id);
        if(stristr($data['roleadmin']->idRole, "2") === false){
           return redirect('/');
        } 
        $data['admins'] = DB::table('users')->where('is_admin', 1)->get();

        if(Auth::user()->is_admin == 1)
        {     

            return view('admin/admins/dashboard',$data);
        }
        else
        {
            return redirect('home')->with('error','You don’t have admin access');
        }  
    }

    public function role($id)
    {

        $data['roleadmin'] = User::find(Auth::user()->id);
        if(stristr($data['roleadmin']->idRole, "2") === false){
           return redirect('/');
        } 
              
    //    $roles=Auth::user()->idRole;
    //    $str_arr = preg_split ("/\,/", $roles);
        
    //    print_r($str_arr);
    $data['admins'] = DB::table('users')->where('id', request()->segment(4) )->first();
    $data['admins']->adminrole = preg_split ("/\,/",$data['admins']->idRole);
    $data['roles'] = DB::table('roles')->get();
    //print_r ( $data['admins']);
    //dd( $data['roles']);
    
 

      if(Auth::user()->is_admin == 1)
      {     
          return view('admin/admins/role',$data);
      }
      else
      {
          return redirect('home')->with('error','You don’t have admin access');
      }  
        
    }

    public function updaterole()
    {
      $log_a = "";
    
      foreach (request()->except('_token') as $key => $value) {
          if(is_array($value))
              $log_a .= $key.",";
          else                    
              $log_a .= $key.",";
      }
      
      
      $idr = array('idRole'=>$log_a);
      DB::table('users')->where('id', request()->segment(4))->update($idr);

      
      return redirect('/my_admin/admins');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        //
    }
}
