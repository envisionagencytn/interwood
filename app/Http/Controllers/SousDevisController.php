<?php

namespace App\Http\Controllers;

use App\Models\Sous_devis;
use Illuminate\Http\Request;

class SousDevisController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sous_devis  $sous_devis
     * @return \Illuminate\Http\Response
     */
    public function show(Sous_devis $sous_devis)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Sous_devis  $sous_devis
     * @return \Illuminate\Http\Response
     */
    public function edit(Sous_devis $sous_devis)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Sous_devis  $sous_devis
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sous_devis $sous_devis)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sous_devis  $sous_devis
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sous_devis $sous_devis)
    {
        //
    }
}
