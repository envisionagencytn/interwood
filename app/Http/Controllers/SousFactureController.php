<?php

namespace App\Http\Controllers;

use App\Models\Sous_facture;
use Illuminate\Http\Request;

class SousFactureController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sous_facture  $sous_facture
     * @return \Illuminate\Http\Response
     */
    public function show(Sous_facture $sous_facture)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Sous_facture  $sous_facture
     * @return \Illuminate\Http\Response
     */
    public function edit(Sous_facture $sous_facture)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Sous_facture  $sous_facture
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sous_facture $sous_facture)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sous_facture  $sous_facture
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sous_facture $sous_facture)
    {
        //
    }
}
