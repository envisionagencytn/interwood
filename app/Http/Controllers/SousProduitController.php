<?php

namespace App\Http\Controllers;

use App\Models\Produit;
use App\Models\Sous_Produit;
use App\Models\Fournisseur;
use App\Models\Produit_facture;

use Illuminate\Http\Request;
use App\Models\User;
use Auth ;


class SousProduitController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roleadmin = User::find(Auth::user()->id);
        if(stristr($roleadmin->idRole, "5") === false){
           return redirect('/');
        } 

        $sousproduits = Sous_Produit::orderBy('id', 'desc')->get()->groupBy('produit_id');
        
       

        $detailsadd = Produit_facture::get();
        //dd($detailsadd );
        
        return view('admin/produits/stock/dashboard', compact('sousproduits','roleadmin','detailsadd'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roleadmin = User::find(Auth::user()->id);
        if(stristr($roleadmin->idRole, "5") === false){
           return redirect('/');
        } 

        $sousproduits = Sous_Produit::get();
        $produits = Produit::get();
        $fournisseurs = Fournisseur::get();

        //dd($produit_facture);
        return view('admin/produits/stock/add_sousproduits', compact('produits','sousproduits','fournisseurs','roleadmin'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd ($request->all());
        $sousproduit = new Sous_Produit;
        $sousproduit->produit_id = request('produit_id');
        $sousproduit->prix = request('prix');
        $sousproduit->quantite = request('quantite');
        $sousproduit->quantite_restant = request('quantite');
        $sousproduit->etat = request('etat');
        $sousproduit->prix_suppl = request('prix_suppl');
        $sousproduit->prix_vente_tn = request('prix_vente_tn');
        $sousproduit->fournisseur_id = request('fournisseur_id');
        $sousproduit->facturer = "0";


        $produit_facture_one = Produit_facture::find(request('produit_id'));
        $produit_facture = Produit_facture::where('produit_id',request('produit_id'))->get();
        
        //dd($produit_facture);
        if ( $produit_facture->isEmpty() ) {
            //dd($produit_facture);
            $produit_facturenew = new Produit_facture;
            $produit_facturenew->produit_id = request('produit_id');
            $produit_facturenew->quantite = request('quantite') ;

            $produit_facturenew->save();

        } else {
            $produit_facturenew = Produit_facture::find(request('produit_id'));
            $produit_facturenew->quantite = $produit_facture_one->quantite + request('quantite') ;
            $produit_facturenew->save();
        }
        
        $sousproduit->save();


        //redirect back to the users list
        return redirect('my_admin/produits/stock');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sous_Produit  $sous_Produit
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $roleadmin = User::find(Auth::user()->id);
        if(stristr($roleadmin->idRole, "5") === false){
           return redirect('/');
        } 

        $sousproduit = Sous_Produit::orderBy('id', 'desc')->where('produit_id',$id)->get();
        
       
        
        return view('admin/produits/stock/show_sousproduits', compact('sousproduit','roleadmin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Sous_Produit  $sous_Produit
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roleadmin = User::find(Auth::user()->id);
        if(stristr($roleadmin->idRole, "5") === false){
           return redirect('/');
        } 

        $sousproduits = Sous_Produit::find($id);
        $produits = Produit::get();
        $fournisseurs = Fournisseur::get();

        return view('admin/produits/stock/edit_sousproduits', compact('produits','sousproduits','fournisseurs','roleadmin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Sous_Produit  $sous_Produit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sous_Produit $id)
    {

        $sousproduit = Sous_Produit::find($id->id);
        $sousproduit->produit_id = request('produit_id');
        $sousproduit->prix = request('prix');
        $sousproduit->quantite = request('quantite');
        $sousproduit->quantite_restant = request('quantite');
        $sousproduit->etat = request('etat');
        $sousproduit->prix_suppl = request('prix_suppl');
        $sousproduit->prix_vente_tn = request('prix_vente_tn');
        $sousproduit->fournisseur_id = request('fournisseur_id');

        $sousproduit->save();
        // redirect back to the users list
        return redirect('my_admin/produits/stock');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sous_Produit  $sous_Produit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sous_Produit $sous_Produit)
    {
        //
    }
}
