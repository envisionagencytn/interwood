<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\User;
use Auth ;


class UsersController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function index()
  {
    
    $data['roleadmin'] = User::find(Auth::user()->id);
        if(stristr($data['roleadmin']->idRole, "1") === false){
           return redirect('/');
        } 

    $data['users'] = DB::table('users')->get();
    $data['fournisseurs'] = DB::table('fournisseurs')->get();
    $data['enterprises'] = DB::table('enterprises')->get();


    return view('admin/user/dashboard', $data);
  }
 
  public function edit($id)
  {
    $roleadmin = User::find(Auth::user()->id);
    if(stristr($roleadmin->idRole, "1") === false){
       return redirect('/');
    } 

    $user = User::find($id);
    //  $data['admins'] = DB::table('users')->where('id',$id)->first();
    $admins = DB::table('users')->where('id', $id)->first();
    // $data['admins']->adminrole = preg_split("/\,/",$data['admins']->idRole);
    //$adminrole = preg_split("/\,/", $admins->idRoleClient);
    //$roles = DB::table('Rolesclient')->get();
    //$roles = DB::table('Roles')->get();
    //return view('admin/user/edit_user', compact('user', 'roles', 'admins', 'adminrole'));
    return view('admin/user/edit_user', compact('user', 'admins','roleadmin'));
  }

  public function update(Request $request, $id)
  {
    $user = User::find($id);
    // print_r($user->idRole);
    $user->name = request('name');
    $user->email = request('email');
    $user->adresse = request('adresse');
    $user->portable = request('portable');
    $user->pays = request('pays');
    $user->is_admin = request('admins');
    //  print_r($user);
    
    DB::table('users')->where('id', $id);

    $user->save();
    // redirect back to the users list
    return redirect('my_admin/users');
  }
}
