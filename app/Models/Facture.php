<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Facture extends Model
{
    use HasFactory;

    public function enterprise()
    {
        return $this->hasOne(Enterprise::class, 'id', 'enterprise_id');
    }
}
