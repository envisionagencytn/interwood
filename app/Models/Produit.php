<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produit extends Model
{
    use HasFactory;

    public function famille()
    {
        return $this->hasOne(Famille::class, 'id', 'famille_id');
    }

    public function sousproduit() 
    {
        return $this->hasOne(Sous_Produit::class);
        //return $this->hasMany(Sous_Produit::class);
    }

    public function produitqte() 
    {
        return $this->hasOne(Produit_facture::class);
        //return $this->hasMany(Sous_Produit::class);
    }
    

   
}
