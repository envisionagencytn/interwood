<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sous_Produit extends Model
{
    use HasFactory;

    public function produit()
    {
        return $this->hasOne(Produit::class, 'id', 'produit_id');
    }

    public function fournisseur()
    {
        return $this->hasOne(Fournisseur::class, 'id', 'fournisseur_id');
    }
}
