<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sous_devis extends Model
{
    use HasFactory;

    public function produit()
    {
        return $this->hasOne(Produit::class, 'id', 'produit_id');
    }

}
