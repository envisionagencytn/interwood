<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInformationToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('portable',300)->default('');
            $table->string('portable2',300)->default('');
            $table->string('fax',300)->default('');
            $table->string('mf',300)->default('');

            $table->string('adresse',300)->default('');
            $table->string('pays',300)->default('');
            $table->boolean('is_admin')->default(0)->nullable();
            $table->string('idRole',100)->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
