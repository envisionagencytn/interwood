<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddChampsToDevisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('devis', function (Blueprint $table) {

            $table->date('delivery_date')->default(NOW());
            $table->float('gross_weight');
            $table->float('net_weight');
            $table->integer('number_pallets');
            $table->string('size_pallets',100);
            $table->float('fumigation');
            $table->float('engraving');
            $table->float('package');
            $table->float('shipment_insurance');
            $table->float('transit');
            $table->float('taxe');
            $table->float('somme_final_tn');
            $table->string('transport',100);
            $table->string('delivery',100);
            $table->text('libre');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('devis', function (Blueprint $table) {
            //
        });
    }
}
