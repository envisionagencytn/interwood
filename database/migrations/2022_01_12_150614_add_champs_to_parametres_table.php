<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddChampsToParametresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parametres', function (Blueprint $table) {
            $table->string('nom_site');
            $table->string('adress');
            $table->string('adress2');
            $table->string('country');
            $table->string('mf');
            $table->string('daoune');
            $table->string('nom_enterprise');
            $table->string('phone');
            $table->string('email');
            $table->string('email2');
            $table->string('website');
            $table->string('bank');
            $table->string('holder');
            $table->string('rib');
            $table->string('iban');
            $table->string('bic');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parametres', function (Blueprint $table) {
            //
        });
    }
}
