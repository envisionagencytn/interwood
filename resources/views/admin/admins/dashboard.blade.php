@extends('admin.master')

@section('content')

@section('title', "Liste d'admins")

 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-wrapper">
     
      <div class="content-body"><!-- List Of All Patients -->

<section>
  <div class="row">
      <div class="col-12">
          <div class="card">
              <div class="card-header">
                  <h2 class="card-title">Liste des Admins</h2>
                  
              </div>
              <div class="card-body collapse show">
            
                  <div class="table-responsive">
                      <table class="table table-striped table-bordered">
                          <thead>
                              <tr>
                                  <th>Nom et Prénom</th>
                                  <th>Rôle</th>
                              </tr>
                          </thead>
                          <tbody>
                              @foreach ($admins as $admin)
                              <tr>
                              <td>{{ $admin->name}}</td>
                                  <td>
                                      <a href="admins/role/{{ $admin->id}}"><i class="ft-edit text-success"></i></a>
                                  </td>
                                </tr>
                              @endforeach
                              
                          </tbody>
                          
                      </table>
                  </div>
              </div>
          </div>
      </div>
  </div>
</section>
      </div>
    </div>
  </div>
  <!-- END: Content-->

@endsection
