@extends('admin.master')

@section('content')

@section('title', 'Gestion des rôles')

 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-wrapper">
     
      <div class="content-body"><!-- List Of All Patients -->

        <section>
            <div class="icon-tabs">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Rôle d'administrateur</h4>
                                <a href="#" class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                <form method="POST" class="add-doctors-tabs icons-tab-steps steps-validation wizard-notification">
                                    @csrf
                                        <!-- step 1 => Personal Details -->
        
                                    <h6 style="margin-bottom:25px"><i class="step-icon la la-user font-medium-3"></i>Rôle d'administrateur <b> "{{ $admins->name}}" </b></h6>
                                        <fieldset>

                                        <div class="row">
                                            <div class="col-md-12">
                                           
                                                @foreach ($roles as $role)    
                                                               
                                                <div class="form-group">
                                                    <div class="d-inline-block custom-control custom-radio mr-1">

                                                   
                                                    @if(in_array($role->id,$admins->adminrole))
                                                 
                                                    <input type="checkbox" name="{{$role->id}}" class="custom-control-input" id="{{$role->id}}" checked="checked">
                                                    @else
                                                     <input type="checkbox" name="{{$role->id}}" class="custom-control-input" id="{{$role->id}}" >
                                                      @endif

                                                      <label class="custom-control-label" for="{{$role->id}}">{{$role->nom}}</label>
                                                    </div>
                                                </div>
                                                @endforeach

                                                
                                            </div>
                                            
                                          
                                        </div>

                                            <div class="row">
                                                  
                                                <div class="col-md-3 offset-9">
                                                    <div class="form-group">
                                                        <button type="submit" class="float-md-right btn btn-danger round btn-min-width mr-1 mb-1">Valider</button>
                                                    </div>
                                                </div>
                                            </div>

                                        </fieldset>
      
        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        

      </div>
    </div>
  </div>
  <!-- END: Content-->

@endsection
