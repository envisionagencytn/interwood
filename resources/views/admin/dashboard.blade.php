@extends('admin.master')

@section('content')
@section('title', 'Tableau de bord')


<!-- BEGIN: Content-->
<div class="app-content content">
        <div class="content-wrapper">
          <div class="content-header row mb-1">
          </div>
          <div class="content-body"><!-- Hospital Info cards -->
  <div class="row">
    <div class="col-xl-3 col-lg-6 col-md-6 col-12">
      <div class="card pull-up">
        <div class="card-content">
          <div class="card-body">
            <div class="media d-flex">
              <div class="align-self-center">
                <i class="la la-cart-plus font-large-2 success"></i>
              </div>
              <div class="media-body text-right">
                <h5 class="text-muted text-bold-500">Produits</h5>
                <h3 class="text-bold-600">{{$produit}}</h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-lg-6 col-md-6 col-12">
      <div class="card pull-up">
        <div class="card-content">
          <div class="card-body">
            <div class="media d-flex">
              <div class="align-self-center">
                <i class="la la-users font-large-2 warning"></i>
              </div>
              <div class="media-body text-right">
                <h5 class="text-muted text-bold-500">Client inscrit</h5>
                <h3 class="text-bold-600">{{$client}}</h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-lg-6 col-md-6 col-12">
      <div class="card pull-up">
        <div class="card-content">
          <div class="card-body">
            <div class="media d-flex">
              <div class="align-self-center">
                <i class="la la-folder-o font-large-2 info"></i>
              </div>
              <div class="media-body text-right">
                <h5 class="text-muted text-bold-500">Devis</h5>
                <h3 class="text-bold-600">{{$devis}}</h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-lg-6 col-md-6 col-12">
      <div class="card pull-up">
        <div class="card-content">
          <div class="card-body">
            <div class="media d-flex">
              <div class="align-self-center">
                <i class="la la-money font-large-2 danger"></i>
              </div>
              <div class="media-body text-right">
                <h5 class="text-muted text-bold-500">Facture</h5>
                <h3 class="text-bold-600">{{$facture}}</h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Hospital Info cards Ends -->
  
  
  
  <!-- Appointment Table -->
  <div class="row match-height">
     
    <div id="recent-appointments" class="col-6 col-md-6">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Dernière devis 
    
          </h4>
          
        </div>
        <div class="card-content mt-1">
          <div class="table-responsive">
              <table id="recent-orders-doctors" class="table table-hover table-xl mb-0">
                  <thead>
              <tr>
                <th class="border-top-0">Numéro devis</th>
                <th class="border-top-0">Clients</th>
                <th class="border-top-0">Time</th>
                <th class="border-top-0"></th>

              </tr>
                  </thead>
                  <tbody>
                  
              @foreach ($devisten as $devis)
              <tr class="pull-up">
                <td class="text-truncate">{{$devis->devis_num}}{{$devis->devis_annee}}</td>
                <td class="text-truncate p-1">
                  {{$devis->enterprise->nom}}
                </td>
                <td class="text-truncate">{{$devis->devis_date}} </td>
                <td>
                  <a href="my_admin/devis/edit/{{$devis->id}}" class="btn btn-sm btn-outline-danger round">Aperçu</a>
                  </td>
              </tr>
              @endforeach

             
              
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>


    <div id="recent-appointments" class="col-6 col-md-6">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Dernière facture
    
          </h4>
          
        </div>
        <div class="card-content mt-1">
          <div class="table-responsive">
              <table id="recent-orders-doctors" class="table table-hover table-xl mb-0">
                  <thead>
              <tr>
                <th class="border-top-0">Numéro facture</th>
                <th class="border-top-0">Clients</th>
                <th class="border-top-0">Time</th>
                <th class="border-top-0"></th>

              </tr>
                  </thead>
                  <tbody>
              
              @foreach ($factureten as $facture)
              <tr class="pull-up">
                <td class="text-truncate">{{$facture->facture_num}}{{$facture->facture_annee}}</td>
                <td class="text-truncate p-1">
                  {{$facture->enterprise->nom}}
                </td>
                <td class="text-truncate">{{$facture->facture_date}} </td>
                <td>
                    <a href="my_admin/devis/edit/{{$facture->id}}" class="btn btn-sm btn-outline-danger round">Aperçu</a>
                  </td>
              </tr>
              @endforeach

             
              
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>

  </div>
  <!-- Appointment Table Ends -->
          </div>
        </div>
      </div>
      <!-- END: Content-->
      



@endsection
