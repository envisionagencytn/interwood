@extends('admin.master')

@section('content')
@section('title', 'Ajouter devis')

 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-wrapper">
     
      <div class="content-body"><!-- List Of All Patients -->

        <section>
            <div class="icon-tabs">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Ajouter devis</h4>
                                <a href="#" class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form method="POST"  enctype="multipart/form-data" class="add-doctors-tabs icons-tab-steps steps-validation wizard-notification">
                                        @csrf
                                        <!-- step 1 => Personal Details -->
        
                                        <h6 style="margin-bottom:25px"><i class="step-icon la la-user font-medium-3"></i>Page devis</h6>
                                        <fieldset>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="enterprise_id">Client:<span class="danger">*</span></label>
                                                        <select class="enterprise_id form-control p-3" name="enterprise_id" required></select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="devis_date">Devis date:<span class="danger">*</span></label>
                                                        <input type="date" class="form-control required" id="devis_date" name="devis_date" required/>
                                                    </div>
                                                </div>


                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="delivery_date">Delivery date:<span class="danger">*</span></label>
                                                        <input type="date" class="form-control required" id="delivery_date" name="delivery_date" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="gross_weight">Gross weight:</label>
                                                        <input type="number" class="form-control required" id="gross_weight" name="gross_weight" step="0.01" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="net_weight">Net weight:</label>
                                                        <input type="number" class="form-control required" id="net_weight" name="net_weight" step="0.01" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="number_pallets">Number of pallets:</label>
                                                        <input type="number" class="form-control required" id="number_pallets" name="number_pallets" />
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="size_pallets">Size pallets:</label>
                                                        <input type="text" class="form-control required" id="size_pallets" name="size_pallets" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="fumigation">Fumigation:</label>
                                                        <input type="number" class="form-control required" id="fumigation" name="fumigation" step="0.01"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="engraving">Engraving:</label>
                                                        <input type="number" class="form-control required" id="engraving" name="engraving" step="0.01" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="package">Package:</label>
                                                        <input type="number" class="form-control required" id="package" name="package" step="0.01"/>
                                                    </div>
                                                </div>


                                               
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="shipment_insurance">Shipment insurance:</label>
                                                        <input type="number" class="form-control required" id="shipment_insurance" name="shipment_insurance" step="0.01"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="transit">Transit:</label>
                                                        <input type="number" class="form-control required" id="transit" name="transit" step="0.01" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="taxe">Taxe:</label>
                                                        <input type="number" class="form-control required" id="taxe" name="taxe" step="0.01"/>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="transport">Mode transport:<span class="danger">*</span></label>
                                                        <select class="form-control required" id="transport" name="transport" required>
                                                            <option value="">---- choisir une option ----</option>
                                                            <option value="Sea">Sea</option>
                                                            <option value="Air">Air</option>
                                                            <option value="Land">Land</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="delivery">Delivery method:<span class="danger">*</span></label>
                                                        <input type="text" class="form-control required" id="delivery" name="delivery" required/>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="libre">Champs de text:<span class="danger">*</span></label>
                                                        <textarea class="form-control required" id="libre" name="libre" required></textarea>
                                                    </div>
                                                </div>


                                               
                                            </div>

                                                  

                                            <table class="table">
                                                <thead>
                                                  <tr style="font-size: 10px">
                                                    <th scope="col" style="width: 33%">Produit</th>
                                                    <th scope="col">Qte</th>
                                                    <th scope="col">Prix TND</th>
                                                    <th scope="col">Remise</th>
                                                    <th scope="col">T. Echange</th>
                                                    <th scope="col">T. Devise</th>
                                                    <th scope="col" style="width:10%">Devise</th>
                                                    <th scope="col" style="width:10%"></th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <tr class="tr_clone">
                                                    <td style="width: 33%">
                                                        <select class="produit_id form-control required" id="produit_id" name="produit_id[]" required>
                                                           
                                                        </select>
                                                    </td>

                                                    <td><input type="number" class="form-control required" id="devis_details_qte" name="devis_details_qte[]" required></td>
                                                    <td><input type="number" class="form-control required" id="devis_details_prix" name="devis_details_prix[]" step="0.01" required></td>
                                                    <td><input type="number" class="form-control required" id="devis_remise" name="devis_remise[]" required></td>
                                                    <td><input type="number" class="form-control required" id="devis_taux" name="devis_taux[]"step="0.01" required></td>
                                                    <td><input type="number" class="form-control required" id="devis_totale" name="devis_totale[]" step="0.01" required></td>
                                                    <td style="width: 10%"><select class="form-control required" id="devis_devise" name="devis_devise[]" required>
                                                        <option value="1">DT</option>
                                                        <option value="2">EURO</option>
                                                        <option value="3">DOLLAR</option>
                                                        <option value="4">GPC</option>
                                                        </select></td>
                                                    <td style="width:10%">
                                                        <input type="button" class="btn btn-info tr_clone_add" value="+" name="add">
                                                        <a href="javascript:;" class="btn btn-danger deleteRow">-</a>
                                                    </td>
                                                   
                                                  </tr>

                                                  
                                                </tbody>
                                              </table>                                            

                                              <div class="row">
                                                <div class="col-md-3 offset-9">
                                                    <div class="form-group">
                                                        <button type="submit" class="float-md-right btn btn-danger round btn-min-width mr-1 mb-1">Valider</button>
                                                    </div>
                                                </div>
                                            </div>
                                            

                                        </fieldset>
      
        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        

      </div>
    </div>
  </div>
  <!-- END: Content-->

  
  <script>
   

    $('.produit_id').select2({
          placeholder: 'Liste produit',
          ajax: {
            url: '/autocomplete/produit',
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nom,
                            id: item.id
                        }
                    })
                };
            },
        }
    });

    
    $("table").on('click','.tr_clone_add' ,function() {
    $('.produit_id').select2("destroy");        
    var $tr = $(this).closest('.tr_clone');
    var $clone = $tr.clone();
    $tr.after($clone);
    $('.produit_id').select2({
          placeholder: 'Liste produit',
          ajax: {
            url: '/autocomplete/produit',
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nom,
                            id: item.id
                        }
                    })
                };
            },
        }
        
    });
    $clone.find('.produit_id').select2('val', '');
    });

    
    $('tbody').on('click', '.deleteRow', function(){
        $(this).parent().parent().remove();        
    });

    
</script>


<script type="text/javascript">
    $('.enterprise_id').select2({
        placeholder: 'Liste enterprise',
        ajax: {
            url: '/autocomplete/enterprise',
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nom,
                            id: item.id
                        }
                    })
                };
            },
        }
    });
</script>


@endsection
