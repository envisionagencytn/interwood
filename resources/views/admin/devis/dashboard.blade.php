@extends('admin.master')

@section('content')
@section('title', 'Gestion de devis')

 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-wrapper">
     
      <div class="content-body"><!-- List Of All Patients -->

<section>
  <div class="row">
      <div class="col-12">
          <div class="card">
              <div class="card-header">
                  <h2 class="card-title">Liste des devis</h2>
                  <div class="heading-elements">
                      <a href="devis/add" class="btn btn-danger round btn-sm"><i class="la la-plus font-small-2"></i>
                          Ajouter Devis</a>
                  </div>
              </div>
              <div class="card-body collapse show">
            
                  <div class="table-responsive">
                      <table class="table table-striped table-bordered">
                          <thead>
                              <tr>
                                <th>Devis Numéro</th>
                                <th>Nom de enterprise</th>
                                <th>Devis Date</th>
                                <th>Actions</th>
                              </tr>
                          </thead>
                          <tbody>
                            @foreach ($devis as $devi)
                              <tr>
                                <td>{{$devi->devis_num}}{{$devi->devis_annee}}</td>
                                <td>{{$devi->enterprise->nom}}</td>
                                <td>{{$devi->devis_date}}</td>
                                <td>
                                  <a href="devis/edit/{{$devi->id}}"><i class="ft-edit text-success"></i></a>
                                  <a onclick="return confirm('Etes-vous sur de vouloir supprimé ?');" href="devis/delete/{{$devi->id}}"><i class="ft-trash-2 text-warning"></i></a>
                                  <a href="devis/print/{{$devi->id}}" target="_blank"><i class="ft-printer text-info"></i></a>    
                                </td>
                                </tr>
                            @endforeach

                              
                          </tbody>
                          
                      </table>
                  </div>
              </div>
          </div>
      </div>
  </div>
</section>
      </div>
    </div>
  </div>
  <!-- END: Content-->

@endsection
