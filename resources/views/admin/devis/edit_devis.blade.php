@extends('admin.master')

@section('content')
@section('title', 'Modifier devis')

 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-wrapper">
     
      <div class="content-body"><!-- List Of All Patients -->

        <section>
            <div class="icon-tabs">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Modifier devis</h4>
                                <a href="#" class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form method="POST"  enctype="multipart/form-data" class="add-doctors-tabs icons-tab-steps steps-validation wizard-notification">
                                        @csrf
                                        <!-- step 1 => Personal Details -->
        
                                        <h6 style="margin-bottom:25px"><i class="step-icon la la-user font-medium-3"></i>Page devis</h6>
                                        <fieldset>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="enterprise_id">Client:<span class="danger">*</span></label>
                                                        <select class="enterprise_id form-control p-3" name="enterprise_id" required>
                                                            <option value="{{$devis->enterprise_id}}" selected>{{$devis->enterprise->nom}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="devis_date">Devis date:<span class="danger">*</span></label>
                                                        <input type="date" class="form-control required" id="devis_date" name="devis_date" value="{{$devis->devis_date}}" required/>
                                                    </div>
                                                </div>


                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="delivery_date">Delivery date:<span class="danger">*</span></label>
                                                        <input type="date" class="form-control required" id="delivery_date" name="delivery_date" value="{{$devis->delivery_date}}" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="gross_weight">Gross weight:</label>
                                                        <input type="number" class="form-control required" id="gross_weight" name="gross_weight" value="{{$devis->gross_weight}}" step="0.01" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="net_weight">Net weight:</label>
                                                        <input type="number" class="form-control required" id="net_weight" name="net_weight" value="{{$devis->net_weight}}" step="0.01" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="number_pallets">Number of pallets:</label>
                                                        <input type="number" class="form-control required" id="number_pallets" name="number_pallets" value="{{$devis->number_pallets}}" />
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="size_pallets">Size pallets:</label>
                                                        <input type="text" class="form-control required" id="size_pallets" name="size_pallets" value="{{$devis->size_pallets}}" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="fumigation">Fumigation:</label>
                                                        <input type="number" class="form-control required" id="fumigation" name="fumigation" value="{{$devis->fumigation}}" step="0.01"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="engraving">Engraving:</label>
                                                        <input type="number" class="form-control required" id="engraving" name="engraving" value="{{$devis->engraving}}" step="0.01" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="package">Package:</label>
                                                        <input type="number" class="form-control required" id="package" name="package" value="{{$devis->package}}" step="0.01"/>
                                                    </div>
                                                </div>


                                               
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="shipment_insurance">Shipment insurance:</label>
                                                        <input type="number" class="form-control required" id="shipment_insurance" name="shipment_insurance" value="{{$devis->shipment_insurance}}" step="0.01"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="transit">Transit:</label>
                                                        <input type="number" class="form-control required" id="transit" name="transit" value="{{$devis->transit}}" step="0.01" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="taxe">Taxe:</label>
                                                        <input type="number" class="form-control required" id="taxe" name="taxe" step="0.01" value="{{$devis->taxe}}"/>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="transport">Mode transport:<span class="danger">*</span></label>
                                                        <select class="form-control required" id="transport" name="transport" required>
                                                            <option value="">---- choisir une option ----</option>
                                                            <option value="Sea" 
                                                            @if ($devis->transport == "Sea")
                                                            selected
                                                            @endif
                                                            >Sea</option>
                                                            <option value="Air"
                                                            @if ($devis->transport == "Air")
                                                            selected
                                                            @endif
                                                            >Air</option>
                                                            <option value="Land"
                                                            @if ($devis->transport == "Land")
                                                            selected
                                                            @endif
                                                            >Land</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="delivery">Delivery method:<span class="danger">*</span></label>
                                                        <input type="text" class="form-control required" id="delivery" name="delivery" value="{{$devis->delivery}}"  required/>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="libre">Champs de text:<span class="danger">*</span></label>
                                                        <textarea class="form-control required" id="libre" name="libre" required>{{$devis->libre}}</textarea>
                                                    </div>
                                                </div>


                                               
                                            </div>


                                            <div class="col-md-1 float-right">
                                                <div class="col-md-1" >
                                                    <input type="button" id="addRowButton" class="btn btn-info tr_clone_add" value="+" name="add">
                                                    
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="row" style="font-size:10px;font-weight:bold">
                                                <div class="col-md-4">Produit</div>
                                                <div class="col-md-1">Qte</div>
                                                <div class="col-md-1">Prix TND</div>
                                                <div class="col-md-1">Remise</div>
                                                <div class="col-md-1">T. Echange </div>
                                                <div class="col-md-1">T. Devise </div>
                                                <div class="col-md-1">Devise</div>
                                    
                                                <div class="col-md-2" >
                                                    
                                                </div>
                                            </div>
                                            <hr>


                                            
                                            <table style="width: 100%" id="tableExample">
                                                <tbody>
                                                    <tr>
                                                        @foreach ($sousdevis as $sousdevi)
                                                        <td style="display:inline-block;width:100%;height: 80px;"> <iframe width="100%" src="/my_admin/devis/detail/edit/{{$sousdevi->id}}" frameborder="0"></iframe></td>
                                                        @endforeach
                                                    </tr>
                                                    <tr>
                                                        <td style="display:inline-block;width:100%;height: 80px;"> <iframe width="100%" src="/my_admin/devis/detail/add" frameborder="0"></iframe></td>
                                                       
                                                    </tr>
                                                </tbody>
                                              
                                            </table>

                                            <script>
                                                $(document).on('click', '#addRowButton', function() {
                                                    var table = $('#tableExample'),
                                                        lastRow = table.find('tbody tr:last'),
                                                        rowClone = lastRow.clone();
                                                
                                                    table.find('tbody').append(rowClone);
                                                });
                                            
                                            </script>

                                                

                                                  
                                                
                                              <div class="row">
                                                <div class="col-md-3 offset-9">
                                                    <div class="form-group">
                                                        <button type="submit" class="float-md-right btn btn-danger round btn-min-width mr-1 mb-1">Valider</button>
                                                    </div>
                                                </div>
                                            </div>
                                            

                                        </fieldset>
      
        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        

      </div>
    </div>
  </div>
  <!-- END: Content-->

  
  <script>
   

    $('.produit_id').select2({
          placeholder: 'Liste produit',
          ajax: {
            url: '/autocomplete/produit',
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nom,
                            id: item.id
                        }
                    })
                };
            },
        }
    });

    $(document).ready(function() {
    $(".produit_id").change(function(){
                //alert($(this).val());
                
                @foreach ($produits as $produit)  
                if ($(this).val() == "@php echo $produit->id ;
                @endphp") {
                //alert($(this).val());
                $("#devis_details_qte").attr({
                    "max" : @php if (!$produit->sousproduit['quantite']) {
                       echo "10" ;
                    } echo $produit->sousproduit['quantite'] ;
                @endphp, 
                    }
                    );
                $("#devis_details_prix").val(@php echo $produit->sousproduit['prix'] ;
                @endphp);
                }   
                @endforeach
            });
    });
    $("table").on('click','.tr_clone_add' ,function() {
    $('.produit_id').select2("destroy");        
    var $tr = $(this).closest('.tr_clone');
    var $clone = $tr.clone();
    $tr.after($clone);
    $('.produit_id').select2({
          placeholder: 'Liste produit',
          ajax: {
            url: '/autocomplete/produit',
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nom,
                            id: item.id
                        }
                    })
                };
            },
        }
        
    });
    $clone.find('.produit_id').select2('val', '');
    });

    
    $('tbody').on('click', '.deleteRow', function(){
        $(this).parent().parent().remove();        
    });

    
</script>


<script type="text/javascript">
    $('.enterprise_id').select2({
        placeholder: 'Liste enterprise',
        ajax: {
            url: '/autocomplete/enterprise',
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nom,
                            id: item.id
                        }
                    })
                };
            },
        }
    });
</script>


@endsection
