<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<style>
    .select2-container .select2-selection--single{height: 37px !important;}
    .select2-container--default .select2-selection--single .select2-selection__rendered{line-height: 35px !important;}
    .select2-container--default .select2-selection--single .select2-selection__arrow{height: 35px !important;}
</style>
<script type="text/javascript" src="//code.jquery.com/jquery-2.2.3.js"></script>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>


<form class="row" method="post" enctype="multipart/form-data">
    @csrf
    <div class="col-md-4">
        <select class="form-control required produit_id " id="produit_id" name="produit_id" required>
            <option value="{{$sousdevis->produit_id}}">{{$sousdevis->produit->nom}}</option>                                          
        </select>
    </div>
    <div class="col-md-1"><input type="number" class="form-control required" id="devis_details_qte" name="devis_details_qte" value="{{$sousdevis->devis_details_qte}}" required></div>
    <div class="col-md-1"><input type="number" class="form-control required" id="devis_details_prix" name="devis_details_prix" step="0.01" onblur="recalculateSum();" value="{{$sousdevis->devis_details_prix}}"  required></div>
    <div class="col-md-1"><input type="number" class="form-control required" id="devis_remise" name="devis_remise" value="{{$sousdevis->devis_remise}}"  required></div>
    <div class="col-md-1"><input type="number" class="form-control required" id="devis_taux" name="devis_taux"step="0.01" onblur="recalculateSum();" value="{{$sousdevis->devis_taux}}"  required></div>
    <div class="col-md-1"><input type="number" class="form-control required" id="devis_totale" name="devis_totale" step="0.01" value="{{$sousdevis->devis_details_prix/$sousdevis->devis_taux}}"  required></div>
    <div class="col-md-1">
        <select class="form-control required" id="devis_devise" name="devis_devise" required>
            <option value="1" 
            @if ($sousdevis->devis_devise == 1)
                selected
            @endif
            >DT</option>
            <option value="2"
            @if ($sousdevis->devis_devise == 2)
                selected
            @endif
            >EURO</option>
            <option value="3"
            @if ($sousdevis->devis_devise == 3)
                selected
            @endif
            >DOLLAR</option>
            <option value="4"
            @if ($sousdevis->devis_devise == 4)
                selected
            @endif
            >GPC</option>
            </select>
    </div>
                                            
        <div class="col-md-1" >
            <input type="submit" class="btn btn-info" value="Valider" name="add">
        </div>
        <div class="col-md-1" >
            <a href="javascript:;" class="btn btn-danger deleteRow">-</a>
        </div>                                                
</form>


<script>
   

    function recalculateSum()
        {
            var num1 = parseFloat(document.getElementById("devis_details_prix").value);
            var num2 = parseFloat(document.getElementById("devis_taux").value);
            document.getElementById("devis_totale").value = (num1 / num2).toFixed(2);
        }
    
    $('.produit_id').select2({
          placeholder: 'Liste produit',
          ajax: {
            url: '/autocomplete/produit',
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nom,
                            id: item.id
                        }
                    })
                };
            },
        }
    });

    
</script>