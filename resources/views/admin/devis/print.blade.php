<!DOCTYPE html>
<html>

<head>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <link rel="stylesheet" href="{{ asset('admin/assets/css/styleprint.css') }}" />

  <title>Proforma Invoice N° {{$devis->devis_num}}/{{$devis->devis_annee}}</title>

</head>

<body>

  <div class="page-header">
    <button type="button" onClick="window.print()" style="background: green">
        PRINT ME!
    </button>
    <div class="row">
    <div class="col-sm-9 titre" style="font-size: 22px">Quote / Proforma Invoice N° {{$devis->devis_num}}/{{$devis->devis_annee}}</div>
    <div class="col-sm-3 logo"><img src="http://127.0.0.1:8002/img/logo.png" alt=""></div>
    <hr >  

   
    <div class="row" >
        <div class="col-sm-7" style="margin-top:10px">
            <h4>Oder reference : {{$devis->devis_num}}/{{$devis->devis_annee}}</h4>
            <h4>Delivery date : {{$devis->delivery_date}}</h4>
            <h4>Nomber of pieces : 654</h4>
            <h4>Gross weight : 140,20 Kg</h4>
            <h4>Net Weight : 120,20 Kg</h4>
            <h4>Number of pallets : 1 </h4>
            <h4>Size of pallets 60 x 80</h4>
        </div>

        <div class="col-sm-5 col-pull-2">
            <h4><b>Date : {{$devis->devis_date}}</b></h4>
            <h4><b>Customer :</b><br>
            {{$devis->enterprise->nom}} 
            <br> {{$devis->enterprise->user->name}}</h4>
            <h4>{{$devis->enterprise->user->adresse}}</h4>
            <h4>{{$devis->enterprise->user->pays}}</h4>
            <h4>Tél: {{$devis->enterprise->user->portable}}</h4>
            <h4>Email: {{$devis->enterprise->user->email}} </h4>
        </div>
    </div>
    

  

<div class="clearfix"></div>


    </div>
  </div>


  <table>

    <thead>
      <tr>
        <td>
          <!--place holder for the fixed-position header-->
          <div class="page-header-space"></div>
        </td>
      </tr>
    </thead>

    <tbody>
      <tr>
        <td>
          <!--*** CONTENT GOES HERE ***-->
          <div class="page">
            <div class="mydata col-sm-12">
                <table class="table" style="width:100%">
                <tr style="text-align: center">
                    <th class="sanscnt">Description</th>
                    <th>Code</th> 
                    <th>Qty</th> 
                    <th>Unit price</th>
                    <th>Disc.</th>
                    <th>Net Unit price</th>
                    <th>Total</th>
                </tr>
                
                        
                
                @php
                $qtefinal = 0 ;
                $totalfinal = 0 ;
                $totalfinal_remise = 0 ;
                @endphp
                @foreach ($sousdevis as $sousdevi)
           
                @if ($sousdevi->devis_devise == 1)
                @php $devis_devise = "DT" @endphp
                @endif
                @if ($sousdevi->devis_devise == 2)
                @php $devis_devise = "€" @endphp
                @endif
                @if ($sousdevi->devis_devise == 3)
                @php $devis_devise = "$" @endphp
                @endif
                @if ($sousdevi->devis_devise == 4)
                @php $devis_devise = "GPC" @endphp
                @endif

                <tr>
                    <td class="sanscnt">        
                        {{$sousdevi->produit->nom}}
                    </td>
                    
                    @php
                        $totalone = $sousdevi->devis_details_prix/$sousdevi->devis_taux;
                        $totalremise = ($totalone/100)*$sousdevi->devis_remise;
                        $totaloneplus = $totalone-$totalremise ;
                        $totaloftotal = $totaloneplus*$sousdevi->devis_details_qte
                    @endphp
                    <td style="text-align: center">{{$sousdevi->produit->code_produit}}</td> 
                    <td style="text-align: center">{{$sousdevi->devis_details_qte}}</td>
                    <td style="text-align: center">{{round($totalone,2)." ".$devis_devise}}</td>
                    <td style="text-align: center">{{$sousdevi->devis_remise}}%</td>
                    <td style="text-align: center">{{round($totaloneplus,2)." ".$devis_devise}}</td>
                    <td style="text-align: right">{{round($totaloftotal,2)." ".$devis_devise}}</td>
                </tr>
                
                @php
                    $total = ($sousdevi->devis_details_qte * $sousdevi->devis_details_prix/$sousdevi->devis_taux) ;
                    $total_remise = ($total)- ($sousdevi->devis_remise/100)*$total ;
                    $totalfinal += $total ;
                    $totalfinal_remise += $total_remise ;
                    $qtefinal += $sousdevi->devis_details_qte
                @endphp
                
                @endforeach
               
                </table>
            </div>
            <hr style="height:4px;border:none;color:#333;background-color:#333;" >
        
            <footer>
                <div class="row">
                    <div class="col-sm-12"><b>Sub-Total ({{$qtefinal}} pieces) </b><span style="text-align: right;float: right;">{{round($totalfinal_remise,2)}} {{ $devis_devise }}</span></div>
                    <hr>
                    <div class="col-sm-12"><b>Fumigation </b><span style="text-align: right;float: right;">{{$devis->fumigation}}{{ $devis_devise }}</span></div>
                    <div class="col-sm-12"><b>Engraving </b><span style="text-align: right;float: right;">{{$devis->engraving}}{{ $devis_devise }}</span></div>
                    <div class="col-sm-12"><b>Package : {{$devis->package}}% </b><span style="text-align: right;float: right;">{{round((($devis->package/100)*$totalfinal_remise),2)}} {{ $devis_devise }}</span></div>
                    <div class="col-sm-12"><b>Shipment</b><span style="text-align: right;float: right;">{{$devis->shipment_insurance}}{{ $devis_devise }}</span></div>
                    <div class="col-sm-12"><b>Transit </b><span style="text-align: right;float: right;">{{$devis->transit}}{{ $devis_devise }}</span></div>
                    <hr>
                    <div class="col-sm-4 font-20"><b>Total </b><span style="text-align: right;float: right;">{{round((($devis->package/100)*$totalfinal_remise)+$totalfinal_remise+$devis->fumigation+$devis->engraving+$devis->shipment_insurance+$devis->transit,2)}} {{ $devis_devise }}</span></div>
                    <div class="col-sm-4 font-20"><b>Taxe </b><span style="text-align: right;float: right;">{{$devis->taxe}}{{ $devis_devise }}</span></div>
                    <div class="col-sm-4 font-20"><b>Final Total </b><span style="text-align: right;float: right;">{{round((($devis->package/100)*$totalfinal_remise)+$totalfinal_remise+$devis->fumigation+$devis->engraving+$devis->shipment_insurance+$devis->transit+$devis->taxe,2)}} {{ $devis_devise }}</span></div>
                    
                    <div class="headernote col-sm-12">Note</div>
                    <div class="col-sm-12 zone-text">
                        <?php $var = str_replace("\n", "<br/>", $devis->libre);?>

                        <?php echo($var) ?>
                        
                    </div>

                    
                    <div class="col-sm-4">
                        <b>Payment method :  Transfer </b> <br>
                        Delivery method: {{$devis->delivery}} <br>
                        Mode of transport: {{$devis->transport}}  <br>
                        Native country: {{$parametre->country}} <br>
                        Destination country: {{$devis->enterprise->user->pays}} <br>
                    </div>
                    <div class="col-sm-8 " style="padding: 0 !important"> 
                        <div class="boxcr">
                            customer signature (preceded by the words "Good for agreement")		
                        </div>
                    </div>

                    <div class="headerbg">
                        <div class="row">
                        <div class="col-sm-4"><b> Head office </b></div>
                        <div class="col-sm-4"><b>Contact information </b></div>
                        <div class="col-sm-4"><b>Bank details </b></div>
                        </div>
                    </div>
                        <div class="row" style="font-size: 10px">
                        <div class="col-sm-4">
                            {{$parametre->adress}} <br>
                            {{$parametre->adress2}} <br>
                            {{$parametre->country}} <br>
                            N° Matricule Fiscale : {{$parametre->mf}} <br>
                            N° Identification en Douane : {{$parametre->daoune}}
                        </div>
                        <div class="col-sm-4">
                          {{$parametre->nom_enterprise}} <br>
                            Phone : {{$parametre->phone}} <br>
                            E-mail : {{$parametre->email}} <br>
                            E-mail : {{$parametre->email2}} <br>
                            Website : {{$parametre->website}}
                        </div>
                        <div class="col-sm-4">
                          {{$parametre->bank}} <br>
                            Bank account holder : <span style="font-size: 6px">{{$parametre->holder}}</span> <br>
                            RIB : {{$parametre->rib}} <br>
                            IBAN : {{$parametre->iban}} <br>
                            BIC : {{$parametre->bic}}
                        </div>
                        </div>

                        <div class="col-sm-12 lastfooter">
                            All our contact details arementionned above <br>
                            Do not send any payments in any bank other than Inter Wood Craft Company <br>
                            Do not answer any e-mails received from any account but interwoodcraft.com  extention<br>					
                        </div>
     
                </div>
            </footer>
          </div>
        </td>
      </tr>
    </tbody>

    <tfoot>
      <tr>
        <td>
          <!--place holder for the fixed-position footer-->
          <div class="page-footer-space"></div>
          
        </td>
      </tr>
    </tfoot>

  </table>

</body>

</html>

