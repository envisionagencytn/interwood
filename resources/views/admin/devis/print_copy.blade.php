Devis num : {{$devis->devis_num}}{{$devis->devis_annee}}<br>
Devis date : {{$devis->devis_date}}<br>

<br><br>

Nom enterprise : {{$devis->enterprise->nom}}<br>
Email enterprise : {{$devis->enterprise->user->email}}<br>
Portable enterprise : {{$devis->enterprise->user->portable}}<br>
Addresse enterprise : {{$devis->enterprise->user->adresse}}<br>
Pays enterprise : {{$devis->enterprise->user->pays}}<br>

<br><br>

Delivery date : {{$devis->delivery_date}}<br>
Gross_weight : {{$devis->gross_weight}}<br>
Net weight : {{$devis->net_weight}}<br>
Number pallets : {{$devis->number_pallets}}<br>
Size pallets : {{$devis->size_pallets}}<br>

Fumigation : {{$devis->fumigation}}<br>
Engraving : {{$devis->engraving}}<br>
Package : {{$devis->package}}% <br>
Shipment_insurance : {{$devis->shipment_insurance}}<br>
Transit : {{$devis->transit}}<br>
Taxe : {{$devis->taxe}}<br>

<br><br><br>

@php
$totalfinal = 0 ;
$totalfinal_remise = 0 ;
@endphp
@foreach ($sousdevis as $sousdevi)

@if ($sousdevi->devis_devise == 1)
@php $devis_devise = "DT" @endphp
@endif
@if ($sousdevi->devis_devise == 2)
@php $devis_devise = "Euro" @endphp
@endif
@if ($sousdevi->devis_devise == 3)
@php $devis_devise = "Dollar" @endphp
@endif
@if ($sousdevi->devis_devise == 4)
@php $devis_devise = "GPC" @endphp
@endif

Nom de produit : {{$sousdevi->produit->nom}}<br>
Qte de produit : {{$sousdevi->devis_details_qte}}<br>
Prix de produit : {{$sousdevi->devis_details_prix." Dinar"}}<br>
Taux de produit : {{$sousdevi->devis_taux}}<br>
Remise de produit : {{$sousdevi->devis_remise}}%<br>
Prix de produit devise: {{$sousdevi->devis_details_prix*$sousdevi->devis_taux." ".$devis_devise}}<br>
Devis de produit : 


{{ $devis_devise }}

<br>
@php
    $total = ($sousdevi->devis_details_qte * $sousdevi->devis_details_prix * $sousdevi->devis_taux) ;
    $total_remise = ($total)- ($sousdevi->devis_remise/100)*$total ;
    $totalfinal += $total ;
    $totalfinal_remise += $total_remise ;

@endphp
Totale  produits : {{$total}} {{ $devis_devise }}<br>
Totale avec remise  produits : {{$total_remise}} {{ $devis_devise }}<br><br>
<hr>

@endforeach
Packaging : {{(($devis->package/100)*$totalfinal_remise)}} <br>
Total de facture produits : {{$totalfinal}} {{ $devis_devise }}<br>
Total de facture avec remise produits : {{$totalfinal_remise}} {{ $devis_devise }} <br>

Total de facture final : {{(($devis->package/100)*$totalfinal_remise)+$totalfinal_remise+$devis->fumigation+$devis->engraving+$devis->shipment_insurance+$devis->transit+$devis->taxe}} {{ $devis_devise }}


