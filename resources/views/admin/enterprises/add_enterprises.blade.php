@extends('admin.master')

@section('content')
@section('title', 'Ajouter enterprise')

 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-wrapper">
     
      <div class="content-body"><!-- List Of All Patients -->

        <section>
            <div class="icon-tabs">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Ajouter enterprise</h4>
                                <a href="#" class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form method="POST"  enctype="multipart/form-data" class="add-doctors-tabs icons-tab-steps steps-validation wizard-notification">
                                        @csrf
                                        <!-- step 1 => Personal Details -->
        
                                        <h6 style="margin-bottom:25px"><i class="step-icon la la-user font-medium-3"></i>Page enterprise</h6>
                                        <fieldset>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="nom">Nom de l'enterprise:<span class="danger">*</span></label>
                                                    <input type="text" class="form-control required" id="nom" name="nom" required/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Nom de client:<span class="danger">*</span></label>
                                                        <input type="text" class="form-control required" id="name" name="name" required/>
                                                    </div>
                                                </div>
                                               
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="email">Email:</label>
                                                    <input type="email" class="form-control required" id="email" name="email" />
                                                    </div>
                                                </div>
                                            
                                               
                                            </div>

                                            <div class="row">
                                                
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="portable">Numéro de téléphone:<span class="danger">*</span></label>
                                                        <input type="text" class="form-control required" id="portable" name="portable" required/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="portable2">Numéro de téléphone 2:</label>
                                                        <input type="text" class="form-control required" id="portable2" name="portable2"/>
                                                    </div>
                                                </div>
                                               
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="fax">Fax :</label>
                                                        <input type="text" class="form-control required" id="fax" name="fax"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="mf">Matricule fiscale:</label>
                                                        <input type="text" class="form-control required" id="mf" name="mf" />
                                                    </div>
                                                </div>
                                               
                                            </div>


                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="pays">Pays:<span class="danger">*</span></label>
                                                    <input type="text" class="form-control required" id="pays" name="pays" required/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="adresse">Addresse:<span class="danger">*</span></label>
                                                        <input type="text" class="form-control required" id="adresse" name="adresse" required/>
                                                    </div>
                                                </div>
                                               
                                            </div>


                                            
                                         

                                            <div class="row">
                                                <div class="col-md-3 offset-9">
                                                    <div class="form-group">
                                                        <button type="submit" class="float-md-right btn btn-danger round btn-min-width mr-1 mb-1">Valider</button>
                                                    </div>
                                                </div>
                                            </div>

                                        </fieldset>
      
        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        

      </div>
    </div>
  </div>
  <!-- END: Content-->

@endsection
