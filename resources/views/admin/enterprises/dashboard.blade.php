@extends('admin.master')

@section('content')
@section('title', 'Gestion de enterprises')

 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-wrapper">
     
      <div class="content-body"><!-- List Of All Patients -->

<section>
  <div class="row">
      <div class="col-12">
          <div class="card">
              <div class="card-header">
                  <h2 class="card-title">Liste des enterprises</h2>
                  <div class="heading-elements">
                      <a href="enterprises/add" class="btn btn-danger round btn-sm"><i class="la la-plus font-small-2"></i>
                          Ajouter enterprise</a>
                  </div>
              </div>
              <div class="card-body collapse show">
            
                  <div class="table-responsive">
                      <table class="table table-striped table-bordered">
                          <thead>
                              <tr>
                                <th>Nom de enterprise</th>
                                <th>Nom de client</th>
                                <th>Actions</th>
                              </tr>
                          </thead>
                          <tbody>
                            @foreach ($enterprise as $enterprises)
                              <tr>
                                <td>{{$enterprises->nom}}</td>
                                <td>{{$enterprises->user->name}}</td>
                                <td>
                               
                                  <a href="enterprises/edit/{{$enterprises->id}}"><i class="ft-edit text-success"></i></a>
                                      <a onclick="return confirm('Etes-vous sur de vouloir supprimé ?');" href="enterprises/delete/{{$enterprises->id}}"><i class="ft-trash-2 text-warning"></i></a>
                                  </td>
                                </tr>
                            @endforeach

                              
                          </tbody>
                          
                      </table>
                  </div>
              </div>
          </div>
      </div>
  </div>
</section>
      </div>
    </div>
  </div>
  <!-- END: Content-->

@endsection
