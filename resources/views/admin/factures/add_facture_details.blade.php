<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<style>
    .select2-container .select2-selection--single{height: 37px !important;}
    .select2-container--default .select2-selection--single .select2-selection__rendered{line-height: 35px !important;}
    .select2-container--default .select2-selection--single .select2-selection__arrow{height: 35px !important;}
</style>
<script type="text/javascript" src="//code.jquery.com/jquery-2.2.3.js"></script>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>


<form class="row" method="post" enctype="multipart/form-data">
    @csrf
    <div class="col-md-4">
        <select class="form-control required produit_id " id="produit_id" name="produit_id" required>
                                                           
        </select>
    </div>
    <div class="col-md-1"><input type="number" class="form-control required" id="facture_details_qte" name="facture_details_qte" required></div>
    <div class="col-md-1"><input type="number" class="form-control required" id="facture_details_prix" name="facture_details_prix" onblur="recalculateSum();" step="0.01" required></div>
    <div class="col-md-1"><input type="number" class="form-control required" id="facture_remise" name="facture_remise" required></div>
    <div class="col-md-1"><input type="number" class="form-control required" id="facture_taux" name="facture_taux"step="0.01" onblur="recalculateSum();" required></div>
    <div class="col-md-1"><input type="number" class="form-control required" id="facture_totale" name="facture_totale" step="0.01" required></div>
    <div class="col-md-1">
        <select class="form-control required" id="facture_devise" name="facture_devise" required>
            <option value="1">DT</option>
            <option value="2">EURO</option>
            <option value="3">DOLLAR</option>
            <option value="4">GPC</option>
            </select>
    </div>
                                            
        <div class="col-md-1" >
            <input type="submit" class="btn btn-info" value="Valider" name="add">
        </div>
        <div class="col-md-1" >
            <a href="javascript:;" class="btn btn-danger deleteRow">-</a>
        </div>                                                
</form>


<script>
    $('.produit_id').change(function(){
    // alert($(this).val());
    @foreach ($produits as $produit) 
    if ($(this).val() == "@php echo $produit->id ;
                @endphp") {
    $('#facture_details_prix').attr('value', '@php echo $produit->sousproduit['prix_vente_tn'] ; @endphp');
    $('#facture_details_qte').attr('max', '@php if (!$produit->produitqte['quantite']) {
                            echo "0" ;
                            } echo $produit->produitqte['quantite'] ;
                            @endphp');
                        }
    
    @endforeach
    });


    function recalculateSum()
        {
           var num1 = parseFloat(document.getElementById("facture_details_prix").value);
            var num2 = parseFloat(document.getElementById("facture_taux").value);
            document.getElementById("facture_totale").value = (num1 / num2).toFixed(2);
        }
    
    $('.produit_id').select2({
          placeholder: 'Liste produit',
          ajax: {
            url: '/autocomplete/produit',
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nom,
                            id: item.id
                        }
                    })
                };
            },
        }
    });

    
</script>