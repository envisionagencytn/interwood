@extends('admin.master')

@section('content')
@section('title', 'Gestion de factures')

 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-wrapper">
     
      <div class="content-body"><!-- List Of All Patients -->

<section>
  <div class="row">
      <div class="col-12">
          <div class="card">
              <div class="card-header">
                  <h2 class="card-title">Liste des factures</h2>
                  <div class="heading-elements">
                      <a href="factures/add" class="btn btn-danger round btn-sm"><i class="la la-plus font-small-2"></i>
                          Ajouter facture</a>
                  </div>
              </div>
              <div class="card-body collapse show">
            
                  <div class="table-responsive">
                      <table class="table table-striped table-bordered">
                          <thead>
                              <tr>
                                <th>Factures Numéro</th>
                                <th>Nom de enterprise</th>
                                <th>factures Date</th>
                                <th>Actions</th>
                              </tr>
                          </thead>
                          <tbody>
                            @foreach ($factures as $facture)
                              <tr>
                                <td>{{$facture->facture_num}}{{$facture->facture_annee}}</td>
                                <td>{{$facture->enterprise->nom}}</td>
                                <td>{{$facture->facture_date}}</td>
                                <td>
                               
                                  <a href="factures/edit/{{$facture->id}}"><i class="ft-edit text-success"></i></a>
                                  <a onclick="return confirm('Etes-vous sur de vouloir supprimé ?');" href="factures/delete/{{$facture->id}}"><i class="ft-trash-2 text-warning"></i></a>
                                  <a href="factures/print/{{$facture->id}}" target="_blank"><i class="ft-printer text-info"></i></a>
                                </td>
                                </tr>
                            @endforeach

                              
                          </tbody>
                          
                      </table>
                  </div>
              </div>
          </div>
      </div>
  </div>
</section>
      </div>
    </div>
  </div>
  <!-- END: Content-->

@endsection
