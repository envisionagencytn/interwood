Devis num : {{$factures->facture_num}}{{$factures->facture_annee}}<br>
Devis date : {{$factures->facture_date}}<br>

<br><br>

Nom enterprise : {{$factures->enterprise->nom}}<br>
Email enterprise : {{$factures->enterprise->user->email}}<br>
Portable enterprise : {{$factures->enterprise->user->portable}}<br>
Addresse enterprise : {{$factures->enterprise->user->adresse}}<br>
Pays enterprise : {{$factures->enterprise->user->pays}}<br>

<br><br>

Delivery date : {{$factures->delivery_date}}<br>
Gross_weight : {{$factures->gross_weight}}<br>
Net weight : {{$factures->net_weight}}<br>
Number pallets : {{$factures->number_pallets}}<br>
Size pallets : {{$factures->size_pallets}}<br>

Fumigation : {{$factures->fumigation}}<br>
Engraving : {{$factures->engraving}}<br>
Package : {{$factures->package}}% <br>
Shipment_insurance : {{$factures->shipment_insurance}}<br>
Transit : {{$factures->transit}}<br>
Taxe : {{$factures->taxe}}<br>

<br><br><br>

@php
$totalfinal = 0 ;
$totalfinal_remise = 0 ;
@endphp
@foreach ($sousfactures as $sousfacture)

@if ($sousfacture->facture_devise == 1)
@php $facture_devise = "DT" @endphp
@endif
@if ($sousfacture->facture_devise == 2)
@php $facture_devise = "Euro" @endphp
@endif
@if ($sousfacture->facture_devise == 3)
@php $facture_devise = "Dollar" @endphp
@endif
@if ($sousfacture->facture_devise == 4)
@php $facture_devise = "GPC" @endphp
@endif

Nom de produit : {{$sousfacture->produit->nom}}<br>
Qte de produit : {{$sousfacture->facture_details_qte}}<br>
Prix de produit : {{$sousfacture->facture_details_prix." Dinar"}}<br>
Taux de produit : {{$sousfacture->facture_taux}}<br>
Remise de produit : {{$sousfacture->facture_remise}}%<br>
Prix de produit devise: {{$sousfacture->facture_details_prix*$sousfacture->facture_taux." ".$facture_devise}}<br>
Devis de produit : 


{{ $facture_devise }}

<br>
@php
    $total = ($sousfacture->facture_details_qte * $sousfacture->facture_details_prix * $sousfacture->facture_taux) ;
    $total_remise = ($total)- ($sousfacture->facture_remise/100)*$total ;
    $totalfinal += $total ;
    $totalfinal_remise += $total_remise ;

@endphp
Totale  produits : {{$total}} {{ $facture_devise }}<br>
Totale avec remise  produits : {{$total_remise}} {{ $facture_devise }}<br><br>
<hr>

@endforeach
Total de facture produits : {{$totalfinal}} {{ $facture_devise }}<br>
Total de facture avec remise produits : {{$totalfinal_remise}} {{ $facture_devise }} <br>

Total de facture final : {{(($factures->package/100)*$totalfinal_remise)+$totalfinal_remise+$factures->fumigation+$factures->engraving+$factures->shipment_insurance+$factures->transit+$factures->taxe}} {{ $facture_devise }}


