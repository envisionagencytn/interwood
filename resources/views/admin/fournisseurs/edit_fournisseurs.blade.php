@extends('admin.master')

@section('content')
@section('title', 'Modifié fournisseur')

 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-wrapper">
     
      <div class="content-body"><!-- List Of All Patients -->

        <section>
            <div class="icon-tabs">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Modifié fournisseur</h4>
                                <a href="#" class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form method="POST"  enctype="multipart/form-data" class="add-doctors-tabs icons-tab-steps steps-validation wizard-notification">
                                        @csrf
                                        <!-- step 1 => Personal Details -->
                                        <h6 style="margin-bottom:25px"><i class="step-icon la la-user font-medium-3"></i>Page fournisseur</h6>
                                        <fieldset>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="nom">Nom de l'fournisseur:<span class="danger">*</span></label>
                                                    <input type="text" class="form-control required" id="nom" name="nom" value="{{$fournisseurs->nom }}" required/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Nom de client:<span class="danger">*</span></label>
                                                        <input type="text" class="form-control required" id="name" name="name" value="{{$fournisseurs->user->name }}" required/>
                                                    </div>
                                                </div>
                                               
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="email">Email:</label>
                                                    <input type="email" class="form-control required" id="email" name="email" value="{{$fournisseurs->user->email }}"/>
                                                    </div>
                                                </div>
                                                
                                               
                                            </div>

                                            <div class="row">
                                               
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="portable">Numéro de téléphone:<span class="danger">*</span></label>
                                                        <input type="text" class="form-control required" id="portable" name="portable" value="{{$fournisseurs->user->portable }}" required/>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="portable">Numéro de téléphone 2:</label>
                                                        <input type="text" class="form-control required" id="portable2" name="portable2" value="{{$fournisseurs->user->portable2 }}" />
                                                    </div>
                                                </div>
                                               
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="fax">Fax :</label>
                                                        <input type="text" class="form-control required" id="fax" name="fax" value="{{$fournisseurs->user->portable2 }}" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="mf">Matricule fiscale:</label>
                                                        <input type="text" class="form-control required" id="mf" name="mf" value="{{$fournisseurs->user->mf }}" />
                                                    </div>
                                                </div>
                                               
                                            </div>



                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="pays">Pays:<span class="danger">*</span></label>
                                                    <input type="text" class="form-control required" id="pays" name="pays" value="{{$fournisseurs->user->pays }}" required/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="adresse">Addresse:<span class="danger">*</span></label>
                                                        <input type="text" class="form-control required" id="adresse" name="adresse" value="{{$fournisseurs->user->adresse }}" required/>
                                                    </div>
                                                </div>
                                               
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="domaineactivite">Domaine d'activité:<span class="danger">*</span></label>
                                                        <select class="form-control required" id="domaineactivite" name="domaineactivite" >
                                                        
                                                            <option value="Matière première" @if ( $fournisseurs->domaineactivite == "Matière première")
                                                                selected
                                                            @endif>Matière première</option>
                                                            <option value="Semi Fini" @if ( $fournisseurs->domaineactivite == "Semi Fini")
                                                                selected
                                                            @endif>Semi Fini</option>
                                                            <option value="Fini" @if ( $fournisseurs->domaineactivite == "Fini")
                                                                selected
                                                            @endif>Fini</option>
                                                            <option value="Produits consommables" @if ( $fournisseurs->domaineactivite == "Produits consommables")
                                                                selected
                                                            @endif>Produits consommables</option>
                                                            <option value="Outils de travail" @if ( $fournisseurs->domaineactivite == "Outils de travail")
                                                                selected
                                                            @endif>Outils de travail</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                               
                                            </div>
                                            
                                         

                                            <div class="row">
                                                <div class="col-md-3 offset-9">
                                                    <div class="form-group">
                                                        <button type="submit" class="float-md-right btn btn-danger round btn-min-width mr-1 mb-1">Valider</button>
                                                    </div>
                                                </div>
                                            </div>

                                        </fieldset>
      
        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        

      </div>
    </div>
  </div>
  <!-- END: Content-->

@endsection
