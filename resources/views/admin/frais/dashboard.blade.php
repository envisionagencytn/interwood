@extends('admin.master')

@section('content')
@section('title', 'Gestion de frais')

 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-wrapper">
     
      <div class="content-body"><!-- List Of All Patients -->

<section>
  <div class="row">
      <div class="col-12">
          <div class="card">
              <div class="card-header">
                  <h2 class="card-title">Liste des frais</h2>
                  <div class="heading-elements">
                      <a href="frais/add" class="btn btn-danger round btn-sm"><i class="la la-plus font-small-2"></i>
                          Ajouter frais</a>
                  </div>
              </div>
              <div class="card-body collapse show">
            
                  <div class="table-responsive">
                      <table class="table table-striped table-bordered">
                          <thead>
                              <tr>
                                <th>Frais</th>
                                <th>Nom de enterprise</th>
                                <th>Frais prix</th>
                                <th>Frais Date</th>
                                <th>Actions</th>
                              </tr>
                          </thead>
                          <tbody>
                            @foreach ($frais as $frai)
                              <tr>
                                <td>{{$frai->nom}}</td>
                                <td>{{$frai->fournisseurs->nom}}</td>
                                <td>{{$frai->prix}} DT</td>
                                <td>{{$frai->date}}</td>
                                <td>
                               
                                  <a href="frais/edit/{{$frai->id}}"><i class="ft-edit text-success"></i></a>
                                  <a onclick="return confirm('Etes-vous sur de vouloir supprimé ?');" href="frais/delete/{{$frai->id}}"><i class="ft-trash-2 text-warning"></i></a>
                                </td>
                                </tr>
                            @endforeach

                              
                          </tbody>
                          
                      </table>
                  </div>
              </div>
          </div>
      </div>
  </div>
</section>
      </div>
    </div>
  </div>
  <!-- END: Content-->

@endsection
