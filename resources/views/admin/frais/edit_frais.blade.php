@extends('admin.master')

@section('content')
@section('title', 'Modifier frais')

 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-wrapper">
     
      <div class="content-body"><!-- List Of All Patients -->

        <section>
            <div class="icon-tabs">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Modifier frais</h4>
                                <a href="#" class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form method="POST"  enctype="multipart/form-data" class="add-doctors-tabs icons-tab-steps steps-validation wizard-notification">
                                        @csrf
                                        <!-- step 1 => Personal Details -->
        
                                        <h6 style="margin-bottom:25px"><i class="step-icon la la-user font-medium-3"></i>Page frais</h6>
                                        <fieldset>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="fournisseur_id">Fournisseur:<span class="danger">*</span></label>
                                                                <select class="fournisseur_id form-control p-3" name="fournisseur_id" required>
                                                                    <option value="{{$frais->fournisseurs->id}}" selected>{{$frais->fournisseurs->nom}}</option>
                                                                </select>


                                                    </div>
                                                </div>
                                               


                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="nom">Nom de produit:<span class="danger">*</span></label>
                                                        <input type="text" class="form-control required" id="nom" name="nom" value="{{$frais->nom}}" required/>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="prix">Prix:</label>
                                                        <input type="number" class="form-control required" id="prix" name="prix" value="{{$frais->prix}}" step="0.01" />
                                                    </div>
                                                </div>
                                               
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="date">Date d'achat:<span class="danger">*</span></label>
                                                        <input type="date" class="form-control required" id="date" name="date" value="{{$frais->date}}" required/>
                                                    </div>
                                                </div>


                                               
                                            </div>


                                                                                    

                                              <div class="row">
                                                <div class="col-md-3 offset-9">
                                                    <div class="form-group">
                                                        <button type="submit" class="float-md-right btn btn-danger round btn-min-width mr-1 mb-1">Valider</button>
                                                    </div>
                                                </div>
                                            </div>
                                            

                                        </fieldset>
      
        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        

      </div>
    </div>
  </div>
  <!-- END: Content-->

  

  <script type="text/javascript">
   
    $('.fournisseur_id').select2({
        placeholder: 'Liste fournisseur',
        ajax: {
            url: '/autocomplete/fournisseur',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nom,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        }
    });
</script>

@endsection
