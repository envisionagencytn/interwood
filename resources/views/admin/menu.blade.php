<!-- BEGIN: Main Menu-->

<div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true">
  <div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
      <li @if (Request::segment(2) == "my_admin")
        class="active"
        @endif
      ><a href="{{ url('/my_admin') }}"><i class="la la-home"></i><span class="menu-title" data-i18n="">Tableau de bord</span></a>
      </li>
    
    <li class=" navigation-header"><span data-i18n="nav.category.professional">Professional</span><i class="la la-ellipsis-h" data-toggle="tooltip" data-placement="right" data-original-title="Professional"></i></li>    
       
        @php if(stristr($roleadmin->idRole, "1") !== false){ @endphp
        <li @if (Request::segment(2) == "users")
          class="active"
          @endif>
            <a href="/my_admin/users">
            <i class="la la-user"></i><span class="menu-title" data-i18n="">Utilisateurs</span>
            </a>
        </li>
      @php }  @endphp
      @php if(stristr($roleadmin->idRole, "2") !== false){ @endphp
        <li @if (Request::segment(2) == "admins")
            class="active"
            @endif>
            <a href="/my_admin/admins">
            <i class="la la-user-secret"></i><span class="menu-title" data-i18n="">Administrateurs</span>
            </a>
        </li>
      @php }  @endphp
   


      <li class=" navigation-header"><span data-i18n="nav.category.professional">Gestion d'enterprise</span><i class="la la-ellipsis-h" data-toggle="tooltip" data-placement="right" data-original-title="Professional"></i></li>
        @php if(stristr($roleadmin->idRole, "3") !== false){ @endphp
        <li @if (Request::segment(2) == "enterprises")
          class="active"
          @endif>
          <a href="/my_admin/enterprises">
          <i class="la la-users"></i><span class="menu-title" data-i18n="">Clients</span>
          </a>
        </li>
        @php }  @endphp
         
      
      @php if(stristr($roleadmin->idRole, "4") !== false){ @endphp
      <li @if (Request::segment(2) == "fournisseurs")
        class="active"
        @endif>
        <a href="/my_admin/fournisseurs">
        <i class="la la-user-times"></i><span class="menu-title" data-i18n="">Fournisseurs</span>
        </a>
      </li>
      @php }  @endphp

      <li class=" navigation-header"><span data-i18n="nav.category.professional">Gestion financière</span><i class="la la-ellipsis-h" data-toggle="tooltip" data-placement="right" data-original-title="Professional"></i></li>

      @php if(stristr($roleadmin->idRole, "5") !== false){ @endphp
      <li class="nav-item
        @if (Request::segment(2) == "produits")
        active open
        @endif ">
        
        <a href="#">
            <i class="la la-dropbox"></i><span class="menu-title" data-i18n="">Produits</span>
        </a>
          <ul class="menu-content">
            <li @if (Request::segment(3) == "categorie")
            class="active"
            @endif>
              <a class="menu-item" href="/my_admin/produits/categorie"><i></i><span>Catégorie</span></a>
            </li> 
            <li @if ((Request::segment(2) == "produits") and (Request::segment(3) == "") )
            class="active"
            @endif>
              <a class="menu-item" href="/my_admin/produits"><i></i><span>Articles</span></a>
            </li> 
            <li @if (Request::segment(3) == "stock")
            class="active"
            @endif>
              <a class="menu-item" href="/my_admin/produits/stock"><i></i><span>Stocks</span></a>
            </li> 
          </ul>
      </li>
      @php }  @endphp
      @php if(stristr($roleadmin->idRole, "6") !== false){ @endphp
      <li @if (Request::segment(2) == "devis")
        class="active"
        @endif>
        <a href="/my_admin/devis">
        <i class="la ft-file-minus"></i><span class="menu-title" data-i18n="">Devis</span>
        </a>
      </li>
      @php }  @endphp

      @php if(stristr($roleadmin->idRole, "7") !== false){ @endphp
      <li @if (Request::segment(2) == "factures")
        class="active"
        @endif>
        <a href="/my_admin/factures">
        <i class="la ft-file-text"></i><span class="menu-title" data-i18n="">Factures</span>
        </a>
      </li>
      @php }  @endphp

      @php if(stristr($roleadmin->idRole, "8") !== false){ @endphp
        <li @if (Request::segment(2) == "frais")
          class="active"
          @endif>
          <a href="/my_admin/frais">
          <i class="la la-money"></i><span class="menu-title" data-i18n="">Frais</span>
          </a>
        </li>
        @php }  @endphp

      <li class=" navigation-header"><span data-i18n="nav.category.professional">Gestion site</span><i class="la la-ellipsis-h" data-toggle="tooltip" data-placement="right" data-original-title="Professional"></i></li>

      @php if(stristr($roleadmin->idRole, "10") !== false){ @endphp
        <li @if (Request::segment(2) == "statistique")
          class="active"
          @endif>
          <a href="/my_admin/statistique">
          <i class="la ft-bar-chart-2"></i><span class="menu-title" data-i18n="">Statistiques</span>
          </a>
        </li>
        @php }  @endphp

        @php if(stristr($roleadmin->idRole, "9") !== false){ @endphp
          <li @if (Request::segment(2) == "parametres")
            class="active"
            @endif>
            <a href="/my_admin/parametres">
            <i class="la ft-globe"></i><span class="menu-title" data-i18n="">Paramètres site web</span>
            </a>
          </li>
          @php }  @endphp

      
    </ul>
  </div>
</div>

<!-- END: Main Menu-->