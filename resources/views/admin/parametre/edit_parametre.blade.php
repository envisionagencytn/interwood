@extends('admin.master')

@section('content')
@section('title', 'parametre')

 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-wrapper">
     
      <div class="content-body"><!-- List Of All Patients -->

        <section>
            <div class="icon-tabs">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Parametre site web</h4>
                                <a href="#" class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form method="POST"  enctype="multipart/form-data" class="add-doctors-tabs icons-tab-steps steps-validation wizard-notification">
                                        @csrf
                                        <!-- step 1 => Personal Details -->
        
                                        <h6 style="margin-bottom:25px"><i class="step-icon la la-user font-medium-3"></i>Page parametre</h6>
                                        <fieldset>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="nom_site">Nom de site web:<span class="danger">*</span></label>
                                                    <input type="text" class="form-control required" id="nom_site" name="nom_site" value="{{$parametre->nom_site}}" required/>
                                                    </div>
                                                </div>                                               
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="nom_enterprise">Nom de l'enterprise:<span class="danger">*</span></label>
                                                        <input type="text" class="form-control required" id="nom_enterprise" name="nom_enterprise" value="{{$parametre->nom_enterprise}}" required/>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="mf">Matricule Fiscale:<span class="danger">*</span></label>
                                                        <input type="text" class="form-control required" id="mf" name="mf" value="{{$parametre->mf}}" required/>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="daoune">Identification en Douane:<span class="danger">*</span></label>
                                                        <input type="text" class="form-control required" id="daoune" name="daoune" value="{{$parametre->daoune}}" required/>
                                                    </div>
                                                </div>
                                               
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="adress">Addresse:<span class="danger">*</span></label>
                                                        <input type="text" class="form-control required" id="adress" name="adress" value="{{$parametre->adress}}" required/>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="adress2">Addresse:<span class="danger">*</span></label>
                                                        <input type="text" class="form-control required" id="adress2" name="adress2" value="{{$parametre->adress2}}" required/>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="country">Country:<span class="danger">*</span></label>
                                                        <input type="text" class="form-control required" id="country" name="country" value="{{$parametre->country}}" required/>
                                                    </div>
                                                </div>
                                               
                                            </div>

                                            


                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="email">Email:<span class="danger">*</span></label>
                                                    <input type="email" class="form-control required" id="email" name="email" value="{{$parametre->email}}" required/>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="email2">Email:<span class="danger">*</span></label>
                                                    <input type="email" class="form-control required" id="email2" name="email2" value="{{$parametre->email2}}" required/>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="phone">Numéro de téléphone:<span class="danger">*</span></label>
                                                        <input type="text" class="form-control required" id="phone" name="phone" value="{{$parametre->phone}}" required/>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="website">Site web:<span class="danger">*</span></label>
                                                        <input type="text" class="form-control required" id="website" name="website" value="{{$parametre->website}}" required/>
                                                    </div>
                                                </div>
                                                
                                               
                                            </div>


                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="bank">Bank:<span class="danger">*</span></label>
                                                    <input type="text" class="form-control required" id="bank" name="bank" value="{{$parametre->bank}}" required/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="holder">Holder:<span class="danger">*</span></label>
                                                        <input type="text" class="form-control required" id="holder" name="holder" value="{{$parametre->holder}}" required/>
                                                    </div>
                                                </div>
                                               
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="rib">RIB:<span class="danger">*</span></label>
                                                        <input type="text" class="form-control required" id="rib" name="rib" value="{{$parametre->rib}}" required/>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="iban">IBAN:<span class="danger">*</span></label>
                                                        <input type="text" class="form-control required" id="iban" name="iban" value="{{$parametre->iban}}" required/>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="bic">BIC:<span class="danger">*</span></label>
                                                        <input type="text" class="form-control required" id="bic" name="bic" value="{{$parametre->bic}}" required/>
                                                    </div>
                                                </div>
                                               
                                            </div>

                                            <div class="row">
                                                <div class="col-md-3 offset-9">
                                                    <div class="form-group">
                                                        <button type="submit" class="float-md-right btn btn-danger round btn-min-width mr-1 mb-1">Valider</button>
                                                    </div>
                                                </div>
                                            </div>

                                        </fieldset>
      
        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        

      </div>
    </div>
  </div>
  <!-- END: Content-->

@endsection
