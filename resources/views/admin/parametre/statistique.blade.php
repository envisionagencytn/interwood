@extends('admin.master')

@section('content')
@section('title', 'Statistique')

 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-wrapper">
     
      <div class="content-body"><!-- List Of All Patients -->

<section>
  <div class="row">
      <div class="col-12">
          <div class="card" style="min-height: 600px">
              <div class="card-header" >
                  <h2 class="card-title">Statistique </h2>
                  <div class="heading-elements">
                      
                  </div>
              </div>
              <div class="card-body collapse show">
            
                  <div class="table-responsive">
                      <h1>Pas de données disponible pour le moment</h1>
                  </div>
              </div>
          </div>
      </div>
  </div>
</section>
      </div>
    </div>
  </div>
  <!-- END: Content-->

@endsection
