@extends('admin.master')

@section('content')
@section('title', 'Gestion de produits')

 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-wrapper">
     
      <div class="content-body"><!-- List Of All Patients -->

<section>
  <div class="row">
      <div class="col-12">
          <div class="card">
              <div class="card-header">
                  <h2 class="card-title">Liste des produits</h2>
                  <div class="heading-elements">
                      <a href="produits/add" class="btn btn-danger round btn-sm"><i class="la la-plus font-small-2"></i>
                          Ajouter produit</a>
                  </div>
              </div>
              <div class="card-body collapse show">
            
                  <div class="table-responsive">
                      <table class="table table-striped table-bordered">
                          <thead>
                              <tr>
                                <th>Nom de produit</th>
                                <th>Categorie</th>
                                <th>Code produit</th>
                                <th>Poid</th>
                                <th>Actions</th>
                              </tr>
                          </thead>
                          <tbody>
                            @foreach ($produits as $produit)
                              <tr>
                                <td>{{$produit->nom}}</td>
                                <td>{{$produit->famille->nom}}</td>
                                <td>{{$produit->code_produit}}</td>
                                <td>{{$produit->poid}} Kg</td>
                                <td>
                               
                                  <a href="produits/edit/{{$produit->id}}"><i class="ft-edit text-success"></i></a>
                                      <a onclick="return confirm('Etes-vous sur de vouloir supprimé ?');" href="produits/delete/{{$produit->id}}"><i class="ft-trash-2 text-warning"></i></a>
                                  </td>
                                </tr>
                            @endforeach
                            
                              
                          </tbody>

                      </table>

                  </div>
                  <div class="d-flex justify-content-center">
                  {{ $produits->links("pagination::bootstrap-4") }}
                  </div>

              </div>
          </div>
      </div>
  </div>
</section>
      </div>
    </div>
  </div>
  <!-- END: Content-->

@endsection
