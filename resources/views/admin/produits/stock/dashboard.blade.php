@extends('admin.master')

@section('content')
@section('title', 'Gestion de stocks')

 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-wrapper">
     
      <div class="content-body"><!-- List Of All Patients -->

<section>
  <div class="row">
      <div class="col-12">
          <div class="card">
              <div class="card-header">
                  <h2 class="card-title">Liste stocks</h2>
                  <div class="heading-elements">
                      <a href="stock/add" class="btn btn-danger round btn-sm"><i class="la la-plus font-small-2"></i>
                          Ajouter stock</a>
                  </div>
              </div>
              <div class="card-body collapse show">
            
                  <div class="table-responsive">
                      <table class="table table-striped table-bordered">
                          <thead>
                              <tr>
                                <th>Nom de produit</th>
                                <th>Quantité restant</th>
                                <th>Prix vente</th>
                                <th>Actions</th>
                              </tr>
                          </thead>
                          <tbody>
                            @foreach ($sousproduits  as $sousproduit)
                            <tr>
                              @php $i= 0 ; @endphp
                              <td>{{$sousproduit[0]->produit->nom}}</td>
                              @foreach ($sousproduit as $sousproduitnv)
                                @php
                                 $i+=$sousproduitnv->quantite_restant   
                                @endphp
                              @endforeach
                              <td>{{$i}}</td>
                              <td>{{$sousproduitnv->prix_vente_tn}} DT</td>
                              <td>
                                <a href="stock/show/{{$sousproduit[0]->produit_id}}"><i class="ft-edit text-success"></i></a>
                              </td>
                            </tr>
                            @endforeach
                            

                              
                          </tbody>
                          
                      </table>
                  </div>
              </div>
          </div>
      </div>
  </div>
</section>
      </div>
    </div>
  </div>
  <!-- END: Content-->

@endsection
