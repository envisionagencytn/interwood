@extends('admin.master')

@section('content')
@section('title', 'Modifier stock')

 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-wrapper">
     
      <div class="content-body"><!-- List Of All Patients -->

        <section>
            <div class="icon-tabs">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Modifier stock</h4>
                                <a href="#" class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form method="POST"  enctype="multipart/form-data" class="add-doctors-tabs icons-tab-steps steps-validation wizard-notification">
                                        @csrf
                                        <!-- step 1 => Personal Details -->
        
                                        <h6 style="margin-bottom:25px"><i class="step-icon la la-user font-medium-3"></i>Page stock</h6>
                                        <fieldset>
                                            <div class="row">
                                                
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="produit_id">Produit:<span class="danger">*</span></label>
                                                            <select class="produit_id  form-control p-3" name="produit_id" required>
                                                                <option value="{{$sousproduits->produit->id}}" selected>{{$sousproduits->produit->nom}}</option>
                                                            </select>
                                                            
                                                    </div>
                                                </div>
                                                

                                               
                                               
                                            </div>

                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="prix">Prix d'achat:<span class="danger">*</span></label>
                                                        <input type="number" class="form-control required" id="prix" name="prix" step="0.01" required value="{{$sousproduits->prix}}"/>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="quantite">Quantité:<span class="danger">*</span></label>
                                                        <input type="number" class="form-control required" id="quantite" name="quantite" disabled required value="{{$sousproduits->quantite}}"/>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="etat">État produit:<span class="danger">*</span></label>
                                                        <select class="form-control required" id="etat" name="etat" required>
                                                            <option value="0" @if ($sousproduits->etat == 0)
                                                                selected
                                                            @endif>Fini</option>
                                                            <option value="1"
                                                            @if ($sousproduits->etat == 1)
                                                                selected
                                                            @endif
                                                            >Non Fini</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="prix_suppl">Frais supplémentaire:<span class="danger">*</span></label>
                                                        <input type="number" class="form-control required" id="prix_suppl" name="prix_suppl" required value="{{$sousproduits->prix_suppl}}" step="0.01"  />
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="prix_vente_tn">Prix de vente:<span class="danger">*</span></label>
                                                        <input type="number" class="form-control required" id="prix_vente_tn" name="prix_vente_tn" required value="{{$sousproduits->prix_vente_tn}}" step="0.01"/>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="fournisseur_id">Fournisseur:<span class="danger">*</span></label>
                                                                <select class="fournisseur_id form-control p-3" name="fournisseur_id" required>
                                                                    <option value="{{$sousproduits->fournisseur->id}}" selected>{{$sousproduits->fournisseur->nom}}</option>
                                                                </select>


                                                    </div>
                                                </div>
                                                

                                            </div>

                                        


                                            <div class="row">
                                                <div class="col-md-3 offset-9">
                                                    <div class="form-group">
                                                        <button type="submit" class="float-md-right btn btn-danger round btn-min-width mr-1 mb-1">Valider</button>
                                                    </div>
                                                </div>
                                            </div>

                                        </fieldset>
      
        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        

      </div>
    </div>
  </div>
  <!-- END: Content-->

     
<script type="text/javascript">
    $('.produit_id').select2({
        placeholder: 'Liste produit',
        ajax: {
            url: '/autocomplete/produit',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nom,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        }
    });

    $('.fournisseur_id').select2({
        placeholder: 'Liste fournisseur',
        ajax: {
            url: '/autocomplete/fournisseur',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nom,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        }
    });
</script>

@endsection
