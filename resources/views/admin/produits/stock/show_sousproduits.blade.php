@extends('admin.master')

@section('content')
@section('title', 'Gestion de stocks')

 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-wrapper">
     
      <div class="content-body"><!-- List Of All Patients -->

<section>
  <div class="row">
      <div class="col-12">
          <div class="card">
              <div class="card-header">
                  <h2 class="card-title">Liste stocks</h2>
                  <div class="heading-elements">
                      
                  </div>
              </div>
              <div class="card-body collapse show">
            
                  <div class="table-responsive">
                      <table class="table table-striped table-bordered">
                          <thead>
                              <tr>
                                <th>Nom de produit</th>
                                <th>Prix d'achat</th>
                                <th>Quantité</th>
                                <th>Quantité restant</th>
                                <th>Frais supp</th>
                                <th>Prix vente</th>
                                <th>Fournisseur</th>
                                <th>Actions</th>
                              </tr>
                          </thead>
                          <tbody>
                              
                            @foreach ($sousproduit as $sousproduitn)
                              <tr>
                                <td>{{$sousproduitn->produit->nom}}</td>
                                <td>{{$sousproduitn->prix}} DT</td>
                                <td>{{$sousproduitn->quantite}}</td>
                                <td>{{$sousproduitn->quantite_restant}}</td>
                                <td>{{$sousproduitn->prix_suppl}} DT</td>
                                <td>{{$sousproduitn->prix_vente_tn}} DT</td>
                                <td>{{$sousproduitn->fournisseur->nom}}</td>
                                <td>
                                  <a href="/my_admin/produits/stock/edit/{{$sousproduitn->id}}"><i class="ft-edit text-success"></i></a>
                                </td>
                                </tr>
                            @endforeach
                            
                              
                          </tbody>
                          
                      </table>
                  </div>
              </div>
          </div>
      </div>
  </div>
</section>
      </div>
    </div>
  </div>
  <!-- END: Content-->

@endsection
