@extends('admin.master')

@section('content')
@section('title', "Liste d'utilisateurs")

 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-wrapper">
     
      <div class="content-body"><!-- List Of All Patients -->

<section>
  <div class="row">
      <div class="col-12">
          <div class="card">
              <div class="card-header">
                  <h2 class="card-title">Liste d'utilisateurs</h2>
                  
              </div>
              <div class="card-body collapse show">
                 
                  <div class="table-responsive">
                      <table class="table table-striped table-bordered">
                          <thead>
                              <tr>
                                  <th>Nom</th>
                                  <th>Email</th>
                                  <th>Numéro de téléphone</th>
                                  <th>Role</th>
                                  <th>Pays</th>
                                  <th>Modifier</th>
                              </tr>
                          </thead>
                          <tbody>

                            @foreach ($users as $user)
                                  <tr>
                                  <td>{{$user->name}}</td>
                                  <td>{{$user->email}}</td>
                                  <td>{{$user->portable}}</td>                                 
                                  <td>
                                    @foreach ($fournisseurs as $fournisseur)
                                    @if ($fournisseur->user_id == $user->id)
                                    Fournisseur 
                                    @endif
                                    @endforeach
                                    @foreach ($enterprises as $enterprise)
                                    @if ($enterprise->user_id == $user->id)
                                    Client
                                    @endif
                                    @endforeach

                                  </td>
                                  <td>{{$user->pays}}</td>
                                  <td>
                                  <a href="user/edit/{{$user->id}}"><i class="ft-edit text-success"></i></a>
                                  </td>
                              </tr>
                              @endforeach
                              
                          </tbody>
                          
                      </table>
                  </div>
              </div>
          </div>
      </div>
  </div>
</section>
      </div>
    </div>
  </div>
  <!-- END: Content-->

@endsection
