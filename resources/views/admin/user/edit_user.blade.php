@extends('admin.master')

@section('content')
@section('title', 'Modifié user')

 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-wrapper">
     
      <div class="content-body">
        <!-- List Of All Patients -->

        <section>
            <div class="icon-tabs">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Modifié user</h4>
                                <a href="#" class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form method="POST" enctype="multipart/form-data" class="add-doctors-tabs icons-tab-steps steps-validation wizard-notification">
                                        @csrf
                                        <!-- step 1 => Personal Details -->
        
                                        <h6 style="margin-bottom:25px"><i class="step-icon la la-user font-medium-3"></i>Page enterprise</h6>
                                        <fieldset>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Nom :<span class="danger">*</span></label>
                                                    <input type="text" class="form-control required" id="name" name="name" value="{{$user->name }}" required />
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="email">mail :<span class="danger">*</span></label>
                                                    <input type="text" class="form-control required" id="email" name="email" value="{{$user->email }}" required/>
                                                    </div>
                                                </div>
                                               
                                            </div>
                                            <div class="row">
                                               

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="portable">Num portable :<span class="danger">*</span></label>
                                                    <input type="text" class="form-control required" id="portable" name="portable" value="{{$user->portable }}" required/>
                                                    </div>
                                                </div>
                                               
                                              

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="pays"> pays :<span class="danger">*</span></label>
                                                    <input type="text" class="form-control required" id="pays" name="pays" value="{{$user->pays }}" required/>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="adresse">Adresse :<span class="danger">*</span></label>
                                                    <input type="text" class="form-control required" id="adresse" name="adresse" value="{{$user->adresse }}" required />
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="admins">Administrateur :<span class="danger">*</span></label>
                                                        <select id="admins" name="admins" class="form-control required" >
                                                            <option value="">--Veuillez choisir une option--</option>
                                                            <option value="1" @if ($user->is_admin === 1)
                                                                selected
                                                            @endif>Oui</option>
                                                            <option value="0" @if ($user->is_admin === 0)
                                                                selected
                                                            @endif>Non</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                     
                                        
                                            

                                            <div class="row">
                                                  
                                                <div class="col-md-3 offset-9">
                                                    <div class="form-group">
                                                        <button type="submit" class="float-md-right btn btn-danger round btn-min-width mr-1 mb-1">Valider</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
      
        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        

      </div>
    </div>
  </div>
  <!-- END: Content-->

@endsection
