<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MyadminController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\EnterpriseController;
use App\Http\Controllers\FournisseurController;
use App\Http\Controllers\FamilleController;
use App\Http\Controllers\ProduitController;
use App\Http\Controllers\SousProduitController;
use App\Http\Controllers\DevisController;
use App\Http\Controllers\FactureController;
use App\Http\Controllers\FraisController;
use App\Http\Controllers\ParametreController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return redirect('/login');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    // return view('dashboard');
    return redirect('/my_admin');
})->name('dashboard');


Route::get('/my_admin', [MyAdminController::class, 'index']);
Route::get('/my_admin/update', [MyAdminController::class, 'update']);

Route::get('/my_admin/users', [UsersController::class, 'index']);
Route::get('/my_admin/user/edit/{id}', [UsersController::class, 'edit']);
Route::post('/my_admin/user/edit/{id}', [UsersController::class, 'update']);

Route::get('/my_admin/admins', [RoleController::class, 'index']);
Route::get('/my_admin/admins/role/{id}', [RoleController::class, 'role']);
Route::post('/my_admin/admins/role/{id}', [RoleController::class, 'updaterole']);
Route::get('/my_admin/admins/add', [RoleController::class, 'add']);
Route::post('/my_admin/admins/add', [RoleController::class, 'AffecterAdmin']);

Route::get('/my_admin/enterprises', [EnterpriseController::class, 'index']);
Route::get('/my_admin/enterprises/add', [EnterpriseController::class, 'create']);
Route::post('/my_admin/enterprises/add', [EnterpriseController::class, 'store']);
Route::get('/my_admin/enterprises/edit/{id}', [EnterpriseController::class, 'edit']);
Route::post('/my_admin/enterprises/edit/{id}', [EnterpriseController::class, 'update']);
Route::get('/my_admin/enterprises/delete/{id}', [EnterpriseController::class, 'destroy']);


Route::get('/my_admin/fournisseurs', [FournisseurController::class, 'index']);
Route::get('/my_admin/fournisseurs/add', [FournisseurController::class, 'create']);
Route::post('/my_admin/fournisseurs/add', [FournisseurController::class, 'store']);
Route::get('/my_admin/fournisseurs/edit/{id}', [FournisseurController::class, 'edit']);
Route::post('/my_admin/fournisseurs/edit/{id}', [FournisseurController::class, 'update']);
Route::get('/my_admin/fournisseurs/delete/{id}', [FournisseurController::class, 'destroy']);

Route::get('/my_admin/produits/categorie', [FamilleController::class, 'index']);
Route::get('/my_admin/produits/categorie/add', [FamilleController::class, 'create']);
Route::post('/my_admin/produits/categorie/add', [FamilleController::class, 'store']);
Route::get('/my_admin/produits/categorie/edit/{id}', [FamilleController::class, 'edit']);
Route::post('/my_admin/produits/categorie/edit/{id}', [FamilleController::class, 'update']);
Route::get('/my_admin/produits/categorie/delete/{id}', [FamilleController::class, 'destroy']);

Route::get('/my_admin/produits', [ProduitController::class, 'index']);
Route::get('/my_admin/produits/add', [ProduitController::class, 'create']);
Route::post('/my_admin/produits/add', [ProduitController::class, 'store']);
Route::get('/my_admin/produits/edit/{id}', [ProduitController::class, 'edit']);
Route::post('/my_admin/produits/edit/{id}', [ProduitController::class, 'update']);
Route::get('/my_admin/produits/delete/{id}', [ProduitController::class, 'destroy']);

Route::get('/my_admin/produits/stock', [SousProduitController::class, 'index']);
Route::get('/my_admin/produits/stock/show/{id}', [SousProduitController::class, 'show']);
Route::get('/my_admin/produits/stock/add', [SousProduitController::class, 'create']);
Route::post('/my_admin/produits/stock/add', [SousProduitController::class, 'store']);
Route::get('/my_admin/produits/stock/edit/{id}', [SousProduitController::class, 'edit']);
Route::post('/my_admin/produits/stock/edit/{id}', [SousProduitController::class, 'update']);
Route::get('/my_admin/produits/stock/delete/{id}', [SousProduitController::class, 'destroy']);

Route::get('autocomplete/produit', [ProduitController::class, 'produitajax']);
Route::get('autocomplete/fournisseur', [ProduitController::class, 'fournisseurajax']);
Route::get('autocomplete/enterprise', [ProduitController::class, 'enterpriseajax']);

Route::get('autocomplete/produit/{id}', [ProduitController::class, 'produitajaxone']);



Route::get('/my_admin/devis', [DevisController::class, 'index']);
Route::get('/my_admin/devis/add', [DevisController::class, 'create']);
Route::post('/my_admin/devis/add', [DevisController::class, 'store']);
Route::get('/my_admin/devis/edit/{id}', [DevisController::class, 'edit']);
Route::post('/my_admin/devis/edit/{id}', [DevisController::class, 'update']);
Route::get('/my_admin/devis/delete/{id}', [DevisController::class, 'destroy']);
Route::get('/my_admin/devis/print/{id}', [DevisController::class, 'print']);

Route::get('/my_admin/devis/detail/add', [DevisController::class, 'adddetail']);
Route::post('/my_admin/devis/detail/add', [DevisController::class, 'adddetailpost']);
Route::get('/my_admin/devis/detail/edit/{id}', [DevisController::class, 'editdetail']);
Route::post('/my_admin/devis/detail/edit/{id}', [DevisController::class, 'editdetailpost']);


Route::get('/my_admin/factures', [FactureController::class, 'index']);
Route::get('/my_admin/factures/add', [FactureController::class, 'create']);
Route::post('/my_admin/factures/add', [FactureController::class, 'store']);
Route::get('/my_admin/factures/edit/{id}', [FactureController::class, 'edit']);
Route::post('/my_admin/factures/edit/{id}', [FactureController::class, 'update']);
Route::get('/my_admin/factures/delete/{id}', [FactureController::class, 'destroy']);
Route::get('/my_admin/factures/print/{id}', [FactureController::class, 'print']);

Route::get('/my_admin/factures/detail/add', [FactureController::class, 'adddetail']);
Route::post('/my_admin/factures/detail/add', [FactureController::class, 'adddetailpost']);
Route::get('/my_admin/factures/detail/edit/{id}', [FactureController::class, 'editdetail']);
Route::post('/my_admin/factures/detail/edit/{id}', [FactureController::class, 'editdetailpost']);



Route::get('/my_admin/frais', [FraisController::class, 'index']);
Route::get('/my_admin/frais/add', [FraisController::class, 'create']);
Route::post('/my_admin/frais/add', [FraisController::class, 'store']);
Route::get('/my_admin/frais/edit/{id}', [FraisController::class, 'edit']);
Route::post('/my_admin/frais/edit/{id}', [FraisController::class, 'update']);
Route::get('/my_admin/frais/delete/{id}', [FraisController::class, 'destroy']);

Route::get('/my_admin/statistique', [ParametreController::class, 'statistique']);
Route::get('/my_admin/parametres', [ParametreController::class, 'index']);
Route::post('/my_admin/parametres', [ParametreController::class, 'create']);
